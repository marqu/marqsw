
// ImAnalyzerDlg.cpp : realization file
//
//#define _CRT_SECURE_NO_WARNINGS

#include "stdafx.h"
#include "ImAnalyzer.h"
#include "ImAnalyzerDlg.h"
#include "afxdialogex.h"
#include <string>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;

// Dialog window CImAnalyzerDlg

CImAnalyzerDlg::CImAnalyzerDlg(CWnd* pParent /*=NULL*/)
: CDialogEx(CImAnalyzerDlg::IDD, pParent),
copies(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CImAnalyzerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST3, listCtrl);
	DDX_Control(pDX, IDC_COMBO1, decList);
}

BEGIN_MESSAGE_MAP(CImAnalyzerDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_EXPLORE, &CImAnalyzerDlg::OnBnClickedExplore)
	ON_BN_CLICKED(IDOK, &CImAnalyzerDlg::OnBnClickedOk)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// Message handlers CImAnalyzerDlg

BOOL CImAnalyzerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Sets the icon of dialog window. Environments makes it automatically,
	// if the main window is not dialog.
	SetIcon(m_hIcon, TRUE);			// Large icon
	SetIcon(m_hIcon, FALSE);		// Small icon

	// Finding of settings file
	CString st;
	noFile = 1;
	st.Format(_T("settings.ini"));
	settings = st;
	HANDLE hf;
	WIN32_FIND_DATA fd;
	hf = FindFirstFile(st, &fd);
	st.ReleaseBuffer();
	listCtrl.SetExtendedStyle(listCtrl.GetExtendedStyle() | LVS_EX_CHECKBOXES);
	// If settings file is found...
	if (hf != INVALID_HANDLE_VALUE)
	{
		// trying to open file...
		CFile file(settings, CFile::modeRead);
		if (file != 0)
		{
			// reading of stored values
			CArchive ar(&file, CArchive::load);
			ar >> colPath >> param1 >> param2 >> copies;
			ar.Close();
			file.Close();
			if (colPath != L"")
			{
				GetDlgItem(IDC_PATH)->SetWindowTextW(colPath);
			}
			if (param1 != "") listCtrl.InsertItem(0, param1);
			if (param2 != "") listCtrl.InsertItem(1, param2);
			noFile = 0;
		}
	}
	decList.AddString(L"ZXing");
	decList.AddString(L"ZBar");
	decList.AddString(L"BarcodeReader");
	decList.SetCurSel(0);
	return TRUE;
}

void CImAnalyzerDlg::OnPaint()
{
	if (IsIconic())
	{
		// Context of drawing
		CPaintDC dc(this);

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Align the icon in center of user's rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

HCURSOR CImAnalyzerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CImAnalyzerDlg::OnBnClickedExplore()
{
	BROWSEINFO inf;
	inf.hwndOwner = AfxGetApp()->m_pMainWnd->m_hWnd;
	inf.pidlRoot = NULL;
	inf.lpszTitle = _T("�������� ����� � ������� �����������...");
	inf.lpfn = NULL;
	inf.lParam = NULL;
	inf.pszDisplayName = NULL;
	inf.ulFlags = BIF_RETURNONLYFSDIRS;
	inf.iImage = NULL;
	InitCommonControls();
	ITEMIDLIST *itls;
	char buf[MAX_PATH];
	// Show "Select folder" dialog
	if ((itls = SHBrowseForFolder(&inf)) != NULL)
		SHGetPathFromIDList(itls, (LPWSTR)buf);
	colPath = (LPCTSTR)buf;
	// Set the selected path in text box
	GetDlgItem(IDC_PATH)->SetWindowText(colPath);
	findDescription(colPath);
}

void CImAnalyzerDlg::findDescription(CString path)
{
	// Define a search filter
	CString mask = _T("/Description.txt");
	CString filter;
	filter.Format(path + mask);
	WIN32_FIND_DATA fd;
	HANDLE hf;
	hf = FindFirstFile(filter, &fd);
	bool found(0);
	int curSel = 1;
	do
	{
		if (hf != INVALID_HANDLE_VALUE)	found = 1;
	} while (FindNextFile(hf, &fd));

	GetDlgItem(IDC_LIST3)->EnableWindow(1);
	listCtrl.DeleteAllItems();
	if (!found)
	{
		listCtrl.InsertItem(0, L"N/A");
		GetDlgItem(IDC_LIST3)->EnableWindow(0);
		return;
	}
	// Read the description file
	ifstream file(filter);
	char open_file[400];
	char* buf = "";
	int index = -1;
	char *next_token1 = NULL;
	do
	{
		index++;
		file.getline(open_file, 400);
		if (index == 0)
		{
			buf = strtok_s(open_file, ".", &next_token1);
			this->copies = atoi(strtok_s(NULL, "\n", &next_token1));
		}
		if (index == 1)
			break;
	} while (!file.eof());
	buf = strtok_s(open_file, "\t", &next_token1);
	param1 = strtok_s(NULL, ":", &next_token1);
	buf = strtok_s(NULL, "\t", &next_token1);
	listCtrl.InsertItem(0, param1);
	param2 = strtok_s(NULL, ":", &next_token1);
	if (param2 != "")listCtrl.InsertItem(1, param2);
	file.close();
}

void ImAnalyzer::setPath(CString path)
{
	this->folderPath = path;
	// Define a search filter
	CString mask = _T("/*.jpg");
	CString filter;
	filter.Format(path + mask);
	WIN32_FIND_DATA fd;
	HANDLE hf;
	hf = FindFirstFile(filter, &fd);
	filter.ReleaseBuffer();
	this->resultVec.clear();
	this->count = 0;
	int curSel = 1;
	// Count all files that match a search filter
	do
	{
		if (hf == INVALID_HANDLE_VALUE)	this->count = 0;
		else this->count++;
	} while (FindNextFile(hf, &fd));
	this->resultVec.resize(count);
}

void ImAnalyzer::setCopies(int cpy)
{
	this->copies = cpy;
}

CString ImAnalyzer::getImage(int number)
{
	CString imgPath;
	CString imNum;
	int actNum = number%this->copies;
	int inum = number / this->copies + 1;
	if (actNum == 1)
		imNum.Format(_T("/img_%d.jpg"), inum);
	else
	{

		if (actNum == 0)
			imNum.Format(_T("/img_%d(%d).jpg"), inum - 1, this->copies);
		else
			imNum.Format(_T("/img_%d(%d).jpg"), inum, actNum);
	}
	if (number > this->count) imgPath = _T("Error!");
	else imgPath = this->folderPath + imNum;
	return imgPath;
}

void ZXing::analyze()
{
	// Defining an initial parameters
	SECURITY_ATTRIBUTES sa;
	PROCESS_INFORMATION pi;
	STARTUPINFO si;
	HANDLE r, w;
	char buf[256];
	DWORD  br;

	memset(&sa, 0, sizeof(sa));
	sa.nLength = sizeof(sa);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;

	// Creating a pipe to read from console output of another process
	CreatePipe(&r, &w, &sa, 128);

	memset(&si, 0, sizeof(si));
	si.cb = sizeof(si);
	si.dwFlags = STARTF_USESTDHANDLES;

	si.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
	si.hStdOutput = w;
	si.hStdError = w;
	CString path = L"zxing.exe ";
	CString fullImName = L"";
	CString bufImName = L"";
	int number = 1;
	char *next_token1 = NULL;
	while (1)
	{
		bufImName = this->getImage(number);
		if (bufImName == "Error!") break;
		// Constructing an image's full path
		fullImName = path + bufImName;

		// Running the decoder process with specified parameters
		DWORD pid = CreateProcess(NULL,
			fullImName.GetBuffer(),
			NULL,
			NULL,
			TRUE,
			CREATE_NO_WINDOW,
			0,
			0,
			&si,
			&pi);
		// Reading of results from pipe
		ReadFile(r, buf, sizeof(buf), &br, NULL);
		this->result = strtok_s(buf, "\r\n", &next_token1);
		// Storing the decoding result
		this->setResult(number - 1);
		showResult(number);
		number++;
	}
	CloseHandle(w);
	CloseHandle(r);
}

void ZXing::setResult(int num)
{
	this->getResult(this->result, num);
}

void ZBar::analyze()
{
	// Defining an initial parameters
	SECURITY_ATTRIBUTES sa;
	PROCESS_INFORMATION pi;
	STARTUPINFO si;
	HANDLE r, w;
	DWORD  br;

	memset(&sa, 0, sizeof(sa));
	sa.nLength = sizeof(sa);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;

	CString path = L"zbarimg.exe ";
	CString fullImName = L"";
	CString bufImName = L"";
	int number = 1;
	char* temp;
	char *next_token1 = NULL;
	while (1)
	{
		char buf[256];
		// Creating a pipe to read from console output of another process
		CreatePipe(&r, &w, &sa, 128);
		memset(&si, 0, sizeof(si));
		si.cb = sizeof(si);
		si.dwFlags = STARTF_USESTDHANDLES;

		si.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
		si.hStdOutput = w;
		si.hStdError = w;
		bufImName = this->getImage(number);
		if (bufImName == "Error!") break;
		// Constructing an image's full path
		fullImName = path + bufImName;

		// Running the decoder process with specified parameters
		DWORD pid = CreateProcess(NULL,
			fullImName.GetBuffer(),
			NULL,
			NULL,
			TRUE,
			CREATE_NO_WINDOW,
			0,
			0,
			&si,
			&pi);
		// Trying to read results from pipe
		if (ReadFile(r, buf, sizeof(char)* 256, &br, NULL))
		{
			temp = strtok_s(buf, ":", &next_token1);
			this->result = strtok_s(NULL, "\r\n", &next_token1);
		}
		else
		{
			this->result = "decode failed";
		}
		// Storing the decoding result
		this->setResult(number - 1);
		showResult(number);
		number++;
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
		// Kill zbarimg processes every 100 steps
		// (this decoder unable to close process by itself for unknown reasons)
		if (number % 100 == 0) system("taskkill /f /im zbarimg.exe");
	}
	// Kill zbarimg processes if any
	system("taskkill /f /im zbarimg.exe");
	CloseHandle(w);
	CloseHandle(r);
}

void ZBar::setResult(int num)
{
	this->getResult(this->result, num);
}

void libQR::analyze()
{
	// Defining an initial parameters
	SECURITY_ATTRIBUTES sa;
	PROCESS_INFORMATION pi;
	STARTUPINFO si;
	HANDLE r, w;
	DWORD  br;

	memset(&sa, 0, sizeof(sa));
	sa.nLength = sizeof(sa);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;

	char *next_token1 = NULL;

	CString path = L"BarcodeReader.exe ";
	CString fullImName = L"";
	CString bufImName = L"";
	int number = 1;
	while (1)
	{
		char buf[256];
		// Creating a pipe to read from console output of another process
		CreatePipe(&r, &w, &sa, 128);
		memset(&si, 0, sizeof(si));
		si.cb = sizeof(si);
		si.dwFlags = STARTF_USESTDHANDLES;

		si.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
		si.hStdOutput = w;
		si.hStdError = w;
		bufImName = this->getImage(number);
		if (bufImName == "Error!") break;
		// Constructing an image's full path
		fullImName = path + bufImName;

		// Running the decoder process with specified parameters
		DWORD pid = CreateProcess(NULL,
			fullImName.GetBuffer(),
			NULL,
			NULL,
			TRUE,
			CREATE_NO_WINDOW,
			0,
			0,
			&si,
			&pi);
		// Trying to read results from pipe
		if (ReadFile(r, buf, sizeof(char)* 256, &br, NULL))
		{
			this->result = strtok_s(buf, "\r\n", &next_token1);
		}
		else
		{
			this->result = "decode failed";
		}
		// Storing the decoding result
		this->setResult(number - 1);
		showResult(number);
		number++;
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
	}
	CloseHandle(w);
	CloseHandle(r);
}

void libQR::setResult(int num)
{
	this->getResult(this->result, num);
}


void ImAnalyzer::getResult(string res, int num)
{
	this->resultVec[num] = res;
}

void ImAnalyzer::showResult(int num)
{
	int rpl;
	FILE* file;
	CString fileName = this->folderPath + L"/Result.txt";
	rpl = fileName.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(fileName);
	fopen_s(&file, lpANSI, "a");
	if (file == 0){ return; }
	CString pic;
	const char* buf;
	int actNum = 0;
	int inum = 0;
	actNum = num%this->copies;
	inum = num / this->copies + 1;
	if (actNum == 1)
		pic.Format(L"img_%d.jpg\t", inum);
	else
	{
		if (actNum == 0)
			pic.Format(L"img_%d(%d).jpg\t", inum - 1, this->copies);
		else
			pic.Format(L"img_%d(%d).jpg\t", inum, actNum);
	}
	lpANSI = W2A(pic);
	fprintf(file, lpANSI);
	buf = this->resultVec[num - 1].c_str();
	fprintf(file, buf);
	fprintf(file, "\n");
	fclose(file);
}

double CImAnalyzerDlg::calculateP(vector<int> data)
{
	int count = 0;
	for (int i = 0; i < data.size(); i++)
	{
		if (data[i] == 1) count++;
	}
	double p = (double)count / (double)data.size();
	return p;
}

void CImAnalyzerDlg::makeCSV()
{
	vector<int>indexes;
	for (int i = 0; i < listCtrl.GetItemCount(); i++)
	{
		if (listCtrl.GetCheck(i)) indexes.push_back(i);
	}
	if (indexes.size() == 0)return;

	char descLine[400], resLine[400];
	char* buf;
	int rpl = colPath.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(colPath);
	CString pth(lpANSI);
	ifstream desc(pth + L"/Description.txt");

	FILE* file;
	CString fileName = pth + L"/resultsToShow.csv";
	lpANSI = W2A(fileName);
	try
	{
		remove(lpANSI);
	}
	catch (...){}
	fopen_s(&file, lpANSI, "w");
	ifstream res(pth + L"/Result.txt");
	vector<string>values;
	bool isTwo = 0;
	CT2CA pszConvertedAnsiString1(param1);
	CT2CA pszConvertedAnsiString2(param2);
	if (indexes.size() > 1)
	{
		fprintf(file, pszConvertedAnsiString1);
		fprintf(file, "; %s", pszConvertedAnsiString2);
		isTwo = 1;
	}
	else
	{
		if (indexes[0] == 0)	fprintf(file, pszConvertedAnsiString1);
		else fprintf(file, pszConvertedAnsiString2);
	}
	fprintf(file, "\n");
	int cnt = -1;
	char *str = new char[1024];
	while (!res.eof())
	{
		res.getline(str, 1024, '\n');
		cnt++;
	}
	res.clear();
	res.seekg(0, ios::beg);
	int index = -1;
	desc.getline(descLine, 400);
	vector<int> fRes;
	fRes.resize(10);
	fRes.clear();
	double p = 0;
	char *loc = setlocale(LC_ALL, "");
	char *next_token1 = NULL;
	for (int i = 0; i < cnt / copies; i++)
	{
		for (int j = 0; j < copies; j++)
		{
			res.getline(resLine, 400);
			desc.getline(descLine, 400);
			buf = strtok_s(descLine, ": ", &next_token1);
			if (isTwo)
			{
				if (j == 0)
					fprintf(file, "%s; ", strtok_s(NULL, "\t", &next_token1));
				buf = strtok_s(NULL, ": ", &next_token1);
				buf = strtok_s(NULL, "\n", &next_token1);
				if (j == 0)
					fprintf(file, "%s; ", buf);
			}
			else
			{
				if (indexes[0] == 0)
				{
					buf = strtok_s(NULL, "\t", &next_token1);
					if (j == 0)
						fprintf(file, "%s; ", buf);
				}
				else
				{
					buf = strtok_s(NULL, "\t", &next_token1);
					buf = strtok_s(NULL, ": ", &next_token1);
					buf = strtok_s(NULL, "\t", &next_token1);
					if (j == 0)
						fprintf(file, "%s; ", buf);
				}
			}
			buf = strtok_s(resLine, "\t", &next_token1);
			string rrr = strtok_s(NULL, "\n", &next_token1);
			size_t found = rrr.find("WORLD!");
			if (found == string::npos) fRes.push_back(0);
			else fRes.push_back(1);
		}
		loc = setlocale(LC_NUMERIC, "ru_RU.utf8");
		p = calculateP(fRes);
		fprintf(file, "%0.3f; \n", p);
		fRes.clear();
		loc = setlocale(LC_ALL, "");
	}
	delete[] str;
	res.close();
	fclose(file);
	desc.close();
}

void CImAnalyzerDlg::OnBnClickedOk()
{
	UpdateData(1);
	GetDlgItem(IDC_PATH)->GetWindowTextW(colPath);
	GetDlgItem(IDC_STAT)->SetWindowText(L"�����������...");
	GetDlgItem(IDOK)->EnableWindow(0);
	switch (decList.GetCurSel())
	{
	case 0:
	{
			  ZXing decoder;
			  decoder.setPath(colPath);
			  decoder.setCopies(copies);
			  decoder.analyze();
	} break;
	case 1:
	{
			  ZBar decoder;
			  decoder.setPath(colPath);
			  decoder.setCopies(copies);
			  decoder.analyze();
	} break;
	case 2:
	{
			  libQR decoder;
			  decoder.setPath(colPath);
			  decoder.setCopies(copies);
			  decoder.analyze();
	} break;
	default:MessageBox(L"��� ������ ��������");
	}

	makeCSV();
	GetDlgItem(IDC_STAT)->SetWindowText(L"������!");
	GetDlgItem(IDOK)->EnableWindow(1);
	UpdateData(0);
}


void CImAnalyzerDlg::OnDestroy()
{
	GetDlgItem(IDC_PATH)->GetWindowTextW(colPath);
	if (noFile)
	{
		CFile crFile(settings, CFile::modeCreate);
	}
	CFile file(settings, CFile::modeWrite);
	CArchive ar(&file, CArchive::store);
	ar << colPath << param1 << param2 << copies;
	ar.Close();
	file.Close();
	CDialogEx::OnDestroy();
}
