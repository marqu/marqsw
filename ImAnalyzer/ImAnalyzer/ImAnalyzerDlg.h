
// ImAnalyzerDlg.h : header file
//

#pragma once

//#define _CRT_SECURE_NO_WARNINGS

#include<fstream>
#include<vector>
#include "afxwin.h"
#include "afxcmn.h"

/** Class to analyze folder with images
* @author	Egor Serov
*/
class ImAnalyzer
{
private:
	std::vector<std::string> resultVec;
	CString folderPath;
	int count;
	int copies;
public:
	/**
	* This method sets the path to folder with images to analyze
	* @param path - path to folder with images to analyze
	*/
	void setPath(CString path);

	/**
	* This method sets the number of copies of image with specific parameters
	* of distortion
	* @param cpy - the number of copies
	*/
	void setCopies(int cpy);

	/**
	* This method returns the full path to image with specific number
	* @param numberOfImage - the number of image to return
	* @return full path to image
	*/
	CString getImage(int numberOfImage);

	/**
	* This method returns the result of analyze an image with specific number
	* @param res - the code decoded from marker
	* @param num - the number of decoded image
	*/
	void getResult(std::string res, int num);

	/**
	* This method is a stub to initialize a process of decoding
	*/
	virtual void analyze() = 0;

	/**
	* This method is a stub to store a result of decoding
	* @param num - the number of decoded image
	*/
	virtual void setResult(int num) = 0;

	/**
	* This method writes a result of decoding to text file
	* @param num - the number of decoded image
	*/
	void showResult(int num);
};

/** Class to analyze folder with images by ZXing decoder
* @author	Egor Serov
*/
class ZXing :public ImAnalyzer
{
private:
	std::string result;
public:
	/**
	* This method initializes a process of decoding
	*/
	void analyze();

	/**
	* This method stores a result of decoding
	* @param num - the number of decoded image
	*/
	void setResult(int num);
};

/** Class to analyze folder with images by ZBar decoder
* @author	Egor Serov
*/
class ZBar :public ImAnalyzer
{
private:
	std::string result;
public:
	/**
	* This method initializes a process of decoding
	*/
	void analyze();

	/**
	* This method stores a result of decoding
	* @param num - the number of decoded image
	*/
	void setResult(int num);
};

/** Class to analyze folder with images by libQR decoder
* @author	Egor Serov
*/
class libQR :public ImAnalyzer
{
private:
	std::string result;
public:
	/**
	* This method initializes a process of decoding
	*/
	void analyze();

	/**
	* This method stores a result of decoding
	* @param num - the number of decoded image
	*/
	void setResult(int num);
};

/** Dialog window class CImAnalyzerDlg
* @author	Egor Serov
*/
class CImAnalyzerDlg : public CDialogEx
{
// Creation
public:
	// default constructor
	CImAnalyzerDlg(CWnd* pParent = NULL);
	CString colPath;
	CString settings;
	bool noFile;
	// Data of the dialog window
	enum { IDD = IDD_IMANALYZER_DIALOG };
protected:
	// Support of DDX/DDV
	virtual void DoDataExchange(CDataExchange* pDX);

// Realization
protected:
	HICON m_hIcon;

	// Functions of message schema
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedExplore();
	afx_msg void OnBnClickedOk();
	afx_msg void OnDestroy();
	CListCtrl listCtrl;
	CString param1, param2;
	int copies;
	CComboBox decList;

	/**
	* This method finds the file with description of image set
	* @param path - path to the folder with an image set
	*/
	void findDescription(CString path);

	/**
	* This method creates the .CSV file with results of image set decoding
	*/
	void makeCSV();

	/**
	* This method calculates the probability of successfull marker decoding
	* @param data - vector of decoding results (1 - succes, 0 - failure) for
	* each image in a set
	*/
	double calculateP(std::vector<int> data);
};
