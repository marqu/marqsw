#include <vector>
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"

using namespace std;
using namespace cv;

class ImProcessorParameters
{
public:
	/*ImProcessorParameters();*/
	vector<double> addParameters(double beginRange, double endRange, int count);
	/*~ImProcessorParameters();*/
};

class ImProcessor
{
private: 
	ImProcessorParameters param;
protected:
	vector<Mat>processedImgs;
	Mat refImg;
public:
	vector<double>params;
	/*ImProcessor();
	~ImProcessor();*/
	bool readRefImage(CString path);
	bool flashOutputImages(CString path,int count);
	virtual bool process();
	bool getParameters(ImProcessorParameters param);
};

class saltAndPepper : public ImProcessor
{
public:
	saltAndPepper();
	bool process();
	~saltAndPepper();
};
class GaussNoise : public ImProcessor
{
public:
	//GaussNoise();
	bool process(int count);
	//~GaussNoise();
};
class imRotate : public ImProcessor
{
public:
	imRotate();
	bool process(int count);
	~imRotate();
};
class GaussBlur : public ImProcessor
{
public:
	GaussBlur();
	bool process();
	~GaussBlur();
};
class illuminationIrregularity : public ImProcessor
{
public:
	illuminationIrregularity();
	bool process();
	~illuminationIrregularity();
};
