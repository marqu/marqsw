
// ImProcDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include <vector>
using namespace std;

struct nameNdll
{
	CString name;
	CString dll;
};

/** Dialog window class CImProcDlg
* @author	Egor Serov
*/
class CImProcDlg : public CDialogEx
{
// Creation
public:
	// Default constructor
	CImProcDlg(CWnd* pParent = NULL);

	// Data of dialog window
	enum { IDD = IDD_IMPROC_DIALOG };
protected:
	// Support of DDX/DDV
	virtual void DoDataExchange(CDataExchange* pDX);
// Realization
protected:
	HICON m_hIcon;
	// Functions of message schema
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CString savPath, selPath;
	CComboBox procList;
	CStatic picCtrl;
	int count;
	vector<nameNdll>dllWeHave;
	CString settings;
	bool noFile;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedImsel();
	afx_msg void OnBnClickedSaveim();
	afx_msg void OnBnClickedSav();
	afx_msg void OnCbnSelchangeProclist();
	afx_msg void OnDestroy();
};
