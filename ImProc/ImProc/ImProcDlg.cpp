
// ImProcDlg.cpp : realization file
//

#include "stdafx.h"
#include "ImProc.h"
#include "ImProcDlg.h"
#include "afxdialogex.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// Dialog window CImProcDlg
typedef HRESULT(WINAPI *importName)(CString*);
typedef HRESULT(WINAPI *importDlg)();
typedef HRESULT(WINAPI *setParam)(CString, CString);

CImProcDlg::CImProcDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CImProcDlg::IDD, pParent)
	, savPath(_T(""))
	, count(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CImProcDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_SAVEPATH, savPath);
	DDX_Control(pDX, IDC_PROCLIST, procList);
}

BEGIN_MESSAGE_MAP(CImProcDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CImProcDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_IMSEL, &CImProcDlg::OnBnClickedImsel)
	ON_BN_CLICKED(IDC_SAV, &CImProcDlg::OnBnClickedSav)
	ON_CBN_SELCHANGE(IDC_PROCLIST, &CImProcDlg::OnCbnSelchangeProclist)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// Message handlers CImProcDlg

BOOL CImProcDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Sets the icon of dialog window. Environments makes it automatically,
	// if the main window is not dialog.
	SetIcon(m_hIcon, TRUE);			// Large icon
	SetIcon(m_hIcon, FALSE);		// Small icon

	// Finding of dll files
	CString filter;
	filter.Format(_T("../Debug/imModificator_*.dll"));
	WIN32_FIND_DATA fd;
	HANDLE hf;
	HINSTANCE hiName;
	CString toAdd = NULL;
	importName getDllName;
	hf = FindFirstFile(filter, &fd);
	filter.ReleaseBuffer();
	noFile = 1;
	nameNdll hlp;
	int curSel = 1;
	do
	{
		if (hf == INVALID_HANDLE_VALUE)	MessageBox(_T("Error"));
		else
		{
			hiName = LoadLibrary(fd.cFileName);
			if (hiName == NULL) continue;
			// Get distortion name from this dll
			getDllName = (importName)GetProcAddress(hiName, "getName");
			if (getDllName == NULL) continue;

			getDllName(&toAdd);
			procList.AddString(toAdd);
			hlp.name = toAdd;
			hlp.dll = fd.cFileName;
			dllWeHave.push_back(hlp);

			FreeLibrary(hiName);
		}
	} while (FindNextFile(hf, &fd));
	toAdd.ReleaseBuffer();
	CString st;
	bool imSel = 0, imSav = 0;
	st.Format(_T("../Debug/set.ini"));
	settings = st;
	hf = FindFirstFile(st, &fd);
	st.ReleaseBuffer();
	if (hf != INVALID_HANDLE_VALUE)
	{
		CFile file(settings, CFile::modeRead);
		if (file != 0)
		{
			CArchive ar(&file, CArchive::load);
			ar >> selPath >> savPath >> curSel;
			ar.Close();
			file.Close();
			if (selPath != L"")
			{
				imSel = 1;
			}
			if (savPath != L"")
			{
				GetDlgItem(IDC_SAVEPATH)->SetWindowTextW(savPath);
				imSav = 1;
			}
			noFile = 0;
		}
	}
	procList.SetCurSel(curSel);
	GetDlgItem(IDC_SAVEPATH)->EnableWindow(imSav);
	GetDlgItem(IDC_SAV)->EnableWindow(imSav);
	GetDlgItem(IDOK)->EnableWindow(imSav);
	procList.EnableWindow(imSel);
	return TRUE;
}

void CImProcDlg::OnPaint()
{
	if (IsIconic())
	{
		// Context of drawing
		CPaintDC dc(this);

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Align the icon in center of user's rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

HCURSOR CImProcDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CImProcDlg::OnBnClickedImsel()
{
	CString filter = _T(" JPG (*.jpg) |*.JPG| BMP (*.bmp) |*.BMP| All Files (*.*)| *.* |";);
	CFileDialog FileOpen(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, filter, NULL);
	if (FileOpen.DoModal() == IDOK)
	{
		selPath = FileOpen.GetPathName();
		CWnd* picW;
		CDC*dc;
		CRect rect;
		picW = GetDlgItem(IDC_PIC);
		dc = picW->GetDC();
		picW->GetClientRect(&rect);
		CImage img;
		img.Load(selPath);
		HDC hDC = dc->GetSafeHdc();
		img.Draw(hDC, rect);
		procList.EnableWindow(1);
		img.Destroy();
	}
}


void CImProcDlg::OnBnClickedOk()
{
	GetDlgItem(IDC_SAVEPATH)->GetWindowTextW(savPath);
	CString choice;
	procList.GetLBText(procList.GetCurSel(), choice);
	int ch;
	for (UINT i = 0; i < dllWeHave.size(); i++)
	{
		if (choice == dllWeHave[i].name) ch = i;
	}

	HINSTANCE hn;
	hn = LoadLibrary(dllWeHave[ch].dll);
	if (hn == NULL) 
		MessageBox(_T("������ ��� �������� dll"));
	else
	{
		setParam sp = (setParam)GetProcAddress(hn, "setParams");
		if (sp == NULL) 
			MessageBox(_T("������!\n�� ������� ��������������� ������� � dll"));
		else
		{
			if (sp(selPath, savPath) == S_OK)
			{
				importDlg idlg = (importDlg)GetProcAddress(hn, "getDialog");
				if (idlg == NULL) 
					MessageBox(_T("������!\n�� ������� ��������������� ������� � dll"));
				else
				{
					idlg();
				}
			}
		}
		FreeLibrary(hn);
	}
	choice.ReleaseBuffer();
}


void CImProcDlg::OnBnClickedSav()
{
	BROWSEINFO inf;
	inf.hwndOwner = AfxGetApp()->m_pMainWnd->m_hWnd;
	inf.pidlRoot = NULL;
	inf.lpszTitle = _T("�������� ����� ��� ���������� �����������...");
	inf.lpfn = NULL;
	inf.lParam = NULL;
	inf.pszDisplayName = NULL;
	inf.ulFlags = BIF_RETURNONLYFSDIRS;
	inf.iImage = NULL;
	InitCommonControls();
	ITEMIDLIST *itls;
	char buf[MAX_PATH];
	if ((itls = SHBrowseForFolder(&inf)) != NULL)
		SHGetPathFromIDList(itls, (LPWSTR)buf);
	GetDlgItem(IDC_SAVEPATH)->SetWindowText((LPCTSTR)buf);
	GetDlgItem(IDOK)->EnableWindow(1);
}

void CImProcDlg::OnCbnSelchangeProclist()
{
	GetDlgItem(IDC_SAVEPATH)->EnableWindow(1);
	GetDlgItem(IDC_SAV)->EnableWindow(1);
}

void CImProcDlg::OnDestroy()
{
	GetDlgItem(IDC_SAVEPATH)->GetWindowTextW(savPath);
	if (noFile)
	{
		CFile crFile(settings, CFile::modeCreate);
	}
	CFile file(settings, CFile::modeWrite);
	CArchive ar(&file, CArchive::store);
	ar << selPath << savPath << procList.GetCurSel();
	ar.Close();
	file.Close();
	vector<nameNdll>().swap(dllWeHave);
	savPath.ReleaseBuffer();
	selPath.ReleaseBuffer();
	settings.ReleaseBuffer();
	m_hBkgrBitmap->unused;
	CDialogEx::OnDestroy();
}
