#include "stdafx.h"
#include "Classes.h"

vector<double> ImProcessorParameters::addParameters(double beginRange, double endRange, int count)
{
	vector<double> Range;
	for (int i = 0; i < count; i++)
	{
		Range.push_back(((endRange - beginRange) / count)*i);
	}
	return Range;
}

bool ImProcessor::readRefImage(CString path)
{
	int rpl = path.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(path);
	this->refImg = imread(lpANSI);
	if (this->refImg.data)
	{
		return true;
	}
	else return false;
}
bool ImProcessor::process()
{
	return true;
}
bool ImProcessor::flashOutputImages(CString path,int count)
{
	int rpl;
	for (int i = 0; i <count; i++)
	{	
		CString buf;
		buf.Format(_T("\\img_%d.jpg"), i+1);
		CString newPath = path + buf;
		rpl = newPath.Replace(_T('\\'), _T('/'));
		USES_CONVERSION;
		LPSTR lpANSI = W2A(newPath);
		imwrite(lpANSI, this->processedImgs[i]);
	}
	return true;
}

bool ImProcessor::getParameters(ImProcessorParameters param)
{
	this->param = param;
	return true;
}

bool GaussNoise::process(int count)
{
	for (int i = 0; i < count; i++)
	{
		Mat tmp = this->refImg;
		
		if (!tmp.empty())
		{
			//Gauss Noise
			//****dwcvtColor(tmp, tmp, 7);
			//****tmp.convertTo(tmp, CV_32FC1);
			/*Mat noise=tmp.clone();
			noise.setTo(0);
			randn(noise, 0, i);
			tmp += noise;*/

			//SaltAndPepper

			/*Mat saltpepper_noise = Mat::zeros(tmp.rows, tmp.cols, CV_8U);
			randu(saltpepper_noise, 0, 255);

			Mat black = saltpepper_noise < 3;
			Mat white = saltpepper_noise > 252;
			
			Mat saltpepper_img = tmp.clone();
			saltpepper_img.setTo(255, white);
			saltpepper_img.setTo(0, black);*/
			
			
		/*	void salt(cv::Mat &image, int n) 
			{*/
				for (int k = 0; k<5; k++) 
				{ // rand() is the MFC random number generator // try qrand() with Qt 
					int i= rand()%tmp.cols;
					int j= rand()%tmp.rows;
					int bi = rand() % tmp.cols;
					int bj = rand() % tmp.rows;
					if (tmp.channels() == 1)
					{ // gray-level image 
						tmp.at<uchar>(j, i) = 255;
						tmp.at<uchar>(bi, bj) = 0;
					}
					else if (tmp.channels() == 3)
					{ // color image 
						tmp.at<Vec3b>(j, i)[0] = 255;
						tmp.at<Vec3b>(bj, bi)[0] = 0;
						tmp.at<Vec3b>(j, i)[1] = 255;
						tmp.at<Vec3b>(bj, bi)[1] = 0;
						tmp.at<Vec3b>(j, i)[2] = 255;
						tmp.at<Vec3b>(bj, bi)[1] = 0;
					} 
				}
			//}

			this->processedImgs.push_back(tmp);
			tmp.release();
		}
		else 
		{
			break; 
			return false;
		}
	}
	return true;
}