#pragma once
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "afxcmn.h"
using namespace cv;

/** Dialog window class II
* @author	Egor Serov
*/
class II : public CDialog
{
	DECLARE_DYNAMIC(II)

public:
	II(CWnd* pParent = NULL);   // Default constructor
	virtual ~II();

	// Data of dialog window
	enum { IDD = IDD_II };
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Support of DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	Mat refImg;
	Mat processed;
	CString imPath;
	CString savePath;
	double param;
	double minb;
	double maxb;
	double mStep;
	int count;
	int copies;

	/**
	* This method applies specific distortion with defined parameters to an image
	* @return modified image
	*/
	Mat process();

	/**
	* This method loads an image with specified full path
	* @return success/failure flag
	*/
	bool readRefImage(CString path);

	/**
	* This method returns parameters for current step of distortion cycle
	* @return parameters of Illumination Irregularity (irregularity constant)
	*/
	double getParameters(int i);

	/**
	* This method saves modified images with description file
	* @param path - full path to directory where results should be saved
	* @param num - number of modified images
	* @param cpy - number of modified image copies with specific parameters of 
	*              distortion
	* @return success/failure flag
	*/
	bool flashOutputImages(CString path, int num,int cpy);
	afx_msg void OnBnClickedOk();
	afx_msg void OnDestroy();
	afx_msg void OnEnKillfocusEdit1();
	afx_msg void OnEnKillfocusEdit4();
	afx_msg void OnEnChangeEdit3();
};
