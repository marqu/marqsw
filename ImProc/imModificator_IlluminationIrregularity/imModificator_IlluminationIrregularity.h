// imModificator_IlluminationIrregularity.dll.h: main header file for DLL imModificator_IlluminationIrregularity.dll
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include stdafx.h before including this file in PCH"
#endif

#include "resource.h"		// basic symbols
#include "II.h"

// CimModificator_IlluminationIrregularitydllApp
// For realization of this class see imModificator_IlluminationIrregularity.dll.cpp
//
extern "C" __declspec(dllexport) HRESULT __stdcall getName(CString* name);
extern "C" __declspec(dllexport) HRESULT __stdcall getDialog();
extern "C" __declspec(dllexport) HRESULT __stdcall setParams(CString refImgPath, CString savImgPath);

class CimModificator_IlluminationIrregularitydllApp : public CWinApp
{
public:
	CimModificator_IlluminationIrregularitydllApp();

// Redefinition
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
