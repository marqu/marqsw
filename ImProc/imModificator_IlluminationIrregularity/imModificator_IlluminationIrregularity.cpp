// imModificator_IlluminationIrregularity.dll.cpp: defines default procedures of initialization DLL.
//

#include "stdafx.h"
#include "imModificator_IlluminationIrregularity.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CimModificator_IlluminationIrregularitydllApp

BEGIN_MESSAGE_MAP(CimModificator_IlluminationIrregularitydllApp, CWinApp)
END_MESSAGE_MAP()


// Creation CimModificator_IlluminationIrregularitydllApp

CimModificator_IlluminationIrregularitydllApp::CimModificator_IlluminationIrregularitydllApp()
{
}

CimModificator_IlluminationIrregularitydllApp theApp;

BOOL CimModificator_IlluminationIrregularitydllApp::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}

II ii;

HRESULT __stdcall getName(CString* name)
{
	*name = _T("Неравномерность освещения");
	return S_OK;
}

HRESULT __stdcall setParams(CString refImgPath, CString savPath)
{
	ii.imPath = refImgPath;
	ii.savePath = savPath;
	return S_OK;
}

HRESULT __stdcall getDialog()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	ii.DoModal();
	return S_OK;
}