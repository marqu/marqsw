// II.cpp: Realization file
//

#include "stdafx.h"
#include "imModificator_IlluminationIrregularity.h"
#include "II.h"
#include "afxdialogex.h"
#include <math.h>

// Dialog window II

IMPLEMENT_DYNAMIC(II, CDialog)

II::II(CWnd* pParent /*=NULL*/)
	: CDialog(II::IDD, pParent)
	, minb(0)
	, maxb(90)
	, count(10)
	, mStep(10)
	, copies(1)
{

}

II::~II()
{
}

void II::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, minb);
	DDX_Text(pDX, IDC_EDIT2, maxb);
	DDX_Text(pDX, IDC_EDIT4, mStep);
	DDX_Text(pDX, IDC_EDIT3, count);
	DDX_Text(pDX, IDC_EDIT8, copies);
}


BEGIN_MESSAGE_MAP(II, CDialog)
	ON_BN_CLICKED(IDOK, &II::OnBnClickedOk)
	ON_WM_DESTROY()
	ON_EN_KILLFOCUS(IDC_EDIT1, &II::OnEnKillfocusEdit1)
	ON_EN_KILLFOCUS(IDC_EDIT4, &II::OnEnKillfocusEdit4)
	ON_EN_CHANGE(IDC_EDIT3, &II::OnEnChangeEdit3)
END_MESSAGE_MAP()


// Message handlers II
bool II::readRefImage(CString path)
{
	int rpl = path.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(path);
	refImg = imread(lpANSI);
	if (refImg.data)
	{
		return true;
	}
	else return false;
}

double II::getParameters(int i)
{
	if (i == 0)return 0.;
	else return mStep;
}

Mat II::process()
{
	Mat tmp = refImg.clone();
	cvtColor(tmp, tmp, 7);
	tmp.convertTo(tmp, CV_8UC1);
	Mat grad = tmp.clone();
	grad.setTo(0);
	bool min = 0;
	if (param < 0)
	{
		min = 1;
		Point2f pt(grad.cols / 2., grad.rows / 2.);
		Mat r = getRotationMatrix2D(pt, 180, 1.0);
		warpAffine(grad, grad, r, Size(grad.cols, grad.rows));
		r.release();
	}
	for (int y = grad.rows - 1; y >= 0; --y)
	{
		unsigned char *const scanLine = grad.ptr<unsigned char>(y);

		for (int x = grad.cols - 1; x >= 0; --x)
		{
			scanLine[x] = scanLine[x] + ((fabs(param) / 200)*y);
		}
	}
	if (min)
	{
		Point2f pt(grad.cols / 2., grad.rows / 2.);
		Mat r = getRotationMatrix2D(pt, -180, 1.0);
		warpAffine(grad, grad, r, Size(grad.cols, grad.rows));
		r.release();
	}
	tmp += grad;
	grad.release();
	return tmp;
}

bool II::flashOutputImages(CString path, int num, int cpy)
{
	int rpl;
	CString buf;
	if (cpy > 1)
		buf.Format(_T("\\img_%d(%d).jpg"), num + 1, cpy);
	else
		buf.Format(_T("\\img_%d.jpg"), num + 1);
	CString newPath = path + buf;
	rpl = newPath.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(newPath);
	imwrite(lpANSI, processed);
	FILE* file;
	CString fileName = path + L"/Description.txt";
	rpl = fileName.Replace(_T('\\'), _T('/'));
	lpANSI = W2A(fileName);
	if ((num == 0) && (cpy == 1)) remove(lpANSI);
	fopen_s(&file, lpANSI, "a");
	if (file != 0)
	{
		if ((num == 0) && (cpy == 1))
		{
			CString mod;
			mod.Format(_T("Modificator: Illumination Irregularity. %d\n"), this->copies);
			lpANSI = W2A(mod);
			fprintf(file, lpANSI);
		}
		CString buf;
		if (cpy > 1)
			buf.Format(_T("img_%d(%d).jpg\tBeta: %.2f\n"), num + 1, cpy, param);
		else
			buf.Format(_T("img_%d.jpg\tBeta: %.2f\n"), num + 1, param);
		lpANSI = W2A(buf);
		fprintf(file, lpANSI);
	}
	fclose(file);
	return true;
}

void II::OnBnClickedOk()
{
	UpdateData(1);
	if (imPath)
	{
		if (minb<-100 || minb>100 || maxb<-100 || maxb>100)
			MessageBox(TEXT("�������� ������� ������ ���� � �������� (-100, 100)"));
		else
		{
			readRefImage(imPath);
			param = minb;
			for (UINT i = 0; i < count; i++)
			{
				param += getParameters(i);
				for (UINT c = 1; c < copies + 1; c++)
				{
					processed = process();
					flashOutputImages(savePath, i, c);
				}
			}
			MessageBox(_T("������� ���������"));
		}
	}
	UpdateData(0);
}

void II::OnDestroy()
{
	refImg.release();
	processed.release();
	CDialog::OnDestroy();
}


void II::OnEnKillfocusEdit1()
{
	UpdateData(1);
	maxb = minb + mStep*(count - 1);
	UpdateData(0);
}


void II::OnEnKillfocusEdit4()
{
	UpdateData(1);
	maxb = minb + mStep*(count - 1);
	UpdateData(0);
}


void II::OnEnChangeEdit3()
{
	UpdateData(1);
	if (count > 0)
	{
		GetDlgItem(IDOK)->EnableWindow(1);
		maxb = minb + mStep*(count - 1);
	}
	else
	{
		MessageBox(_T("���������� ����������� ������ ���� ������ 0"));
		GetDlgItem(IDOK)->EnableWindow(0);
	}
	UpdateData(0);
}
