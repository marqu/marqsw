// imModificator_SaltAndPepper.h: main header file for DLL imModificator_SaltAndPepper
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include stdafx.h before including this file in PCH"
#endif

#include "resource.h"		// basic symbols
#include "snp.h"

// CimModificator_SaltAndPepperApp
// For realization of this class see imModificator_SaltAndPepper.cpp
//
extern "C" __declspec(dllexport) HRESULT __stdcall getName(CString* name);
extern "C" __declspec(dllexport) HRESULT __stdcall getDialog();
extern "C" __declspec(dllexport) HRESULT __stdcall setParams(CString refImgPath,CString savImgPath);

class CimModificator_SaltAndPepperApp : public CWinApp
{
public:
	CimModificator_SaltAndPepperApp();

// Redefinition
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};