// imModificator_SaltAndPepper.cpp: defines default procedures of initialization DLL.
//

#include "stdafx.h"
#include "imModificator_SaltAndPepper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CimModificator_SaltAndPepperApp

BEGIN_MESSAGE_MAP(CimModificator_SaltAndPepperApp, CWinApp)
END_MESSAGE_MAP()

snp SP;

// Creation CimModificator_SaltAndPepperApp

CimModificator_SaltAndPepperApp::CimModificator_SaltAndPepperApp()
{
}

CimModificator_SaltAndPepperApp theApp;

BOOL CimModificator_SaltAndPepperApp::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}

HRESULT __stdcall getName(CString* name)
{
	*name = _T("��� ���� � �����");
	return S_OK;
}

HRESULT __stdcall setParams(CString refImgPath, CString savPath)
{
	SP.imPath = refImgPath;
	SP.savePath = savPath;
	return S_OK;
}

HRESULT __stdcall getDialog()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	SP.DoModal();
	return S_OK;
}