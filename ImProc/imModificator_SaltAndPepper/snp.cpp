// snp.cpp: Realization file
//

#include "stdafx.h"
#include "imModificator_SaltAndPepper.h"
#include "snp.h"
#include "afxdialogex.h"


// Dialog window snp

IMPLEMENT_DYNAMIC(snp, CDialog)

snp::snp(CWnd* pParent /*=NULL*/)
	: CDialog(snp::IDD, pParent)
	, minW(0)
	, maxW(90)
	, minP(0)
	, maxP(90)
	, count(10)
	, sStep(10)
	, pStep(10)
	, copies(1)
{
}

snp::~snp()
{
}

void snp::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, minW);
	DDX_Text(pDX, IDC_EDIT3, maxW);
	DDX_Text(pDX, IDC_EDIT2, minP);
	DDX_Text(pDX, IDC_EDIT4, maxP);
	DDX_Text(pDX, IDC_EDIT5, sStep);
	DDX_Text(pDX, IDC_EDIT6, pStep);
	DDX_Text(pDX, IDC_EDIT7, count);
	DDX_Text(pDX, IDC_EDIT8, copies);
}


BEGIN_MESSAGE_MAP(snp, CDialog)
	ON_BN_CLICKED(IDOK, &snp::OnBnClickedOk)
	ON_WM_DESTROY()
	ON_EN_KILLFOCUS(IDC_EDIT1, &snp::OnEnKillfocusEdit1)
	ON_EN_KILLFOCUS(IDC_EDIT5, &snp::OnEnKillfocusEdit5)
	ON_EN_KILLFOCUS(IDC_EDIT2, &snp::OnEnKillfocusEdit2)
	ON_EN_KILLFOCUS(IDC_EDIT6, &snp::OnEnKillfocusEdit6)
	ON_EN_CHANGE(IDC_EDIT7, &snp::OnEnChangeEdit7)
END_MESSAGE_MAP()


// Message handlers snp
bool snp::readRefImage(CString path)
{
	int rpl = path.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(path);
	refImg = imread(lpANSI);
	if (refImg.data)
	{
		return true;
	}
	else return false;
}

SaltNPepper snp::getParameters(int i)
{
	SaltNPepper params;
	if (i == 0)
	{
		params.salt = 0.;
		params.pepper = 0.;
	}
	else
	{
		params.salt = sStep;
		params.pepper = pStep;
	}
	return params;
}

Mat snp::process()
{
	Mat tmp = refImg.clone();
	for (UINT k = 0; k < param.salt; k++)
	{
		int z = rand() % tmp.cols;
		int j = rand() % tmp.rows;
		if (tmp.channels() == 1)
		{
			tmp.at<uchar>(j, z) = 255;
		}
		else if (tmp.channels() == 3)
		{
			tmp.at<Vec3b>(j, z)[0] = 255;
			tmp.at<Vec3b>(j, z)[1] = 255;
			tmp.at<Vec3b>(j, z)[2] = 255;
		}
	}
	for (UINT u = 0; u < param.pepper; u++)
	{
		int bi = rand() % tmp.cols;
		int bj = rand() % tmp.rows;
		if (tmp.channels() == 1)
		{
			tmp.at<uchar>(bi, bj) = 0;
		}
		else if (tmp.channels() == 3)
		{
			tmp.at<Vec3b>(bj, bi)[0] = 0;
			tmp.at<Vec3b>(bj, bi)[1] = 0;
			tmp.at<Vec3b>(bj, bi)[2] = 0;
		}
	}
	return tmp;
}

bool snp::flashOutputImages(CString path, int num, int cpy)
{
	int rpl;
	CString buf;
	if (cpy > 1)
		buf.Format(_T("\\img_%d(%d).jpg"), num + 1, cpy);
	else
		buf.Format(_T("\\img_%d.jpg"), num + 1);
	CString newPath = path + buf;
	rpl = newPath.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(newPath);
	imwrite(lpANSI, processed);
	FILE* file;
	CString fileName = path + L"/Description.txt";
	rpl = fileName.Replace(_T('\\'), _T('/'));
	lpANSI = W2A(fileName);
	if ((num == 0) && (cpy == 1)) remove(lpANSI);
	fopen_s(&file, lpANSI, "a");
	if (file != 0)
	{
		if ((num == 0) && (cpy == 1))
		{
			CString mod;
			mod.Format(_T("Modificator: Salt & Pepper Noise. %d\n"), this->copies);
			lpANSI = W2A(mod);
			fprintf(file, lpANSI);
		}
		CString buf;
		if (cpy > 1)
			buf.Format(_T("img_%d(%d).jpg\tSalt: %d\tPepper: %d\n"), 
					   num + 1, cpy, param.salt, param.pepper);
		else
			buf.Format(_T("img_%d.jpg\tSalt: %d\tPepper: %d\n"), num + 1, 
					   param.salt, param.pepper);
		lpANSI = W2A(buf);
		fprintf(file, lpANSI);
	}
	fclose(file);
	return true;
}

void snp::OnBnClickedOk()
{
	UpdateData(1);
	if (imPath)
	{
		if ((minW >= 0) && (maxW >= 0) && (minP >= 0) && (maxP >= 0))
		{
			readRefImage(imPath);
			param.salt = minW;
			param.pepper = minP;
			for (UINT i = 0; i < count; i++)
			{
				param.salt += getParameters(i).salt;
				param.pepper += getParameters(i).pepper;
				for (UINT c = 1; c < copies + 1; c++)
				{
					processed = process();
					flashOutputImages(savePath, i, c);
				}
			}
			MessageBox(_T("������� ���������"));
		}
		else MessageBox(_T("���������� ������ ������ ���� ������ 0"));
	}
	UpdateData(0);
}


void snp::OnDestroy()
{
	refImg.release();
	processed.release();
	CDialog::OnDestroy();
}


void snp::OnEnKillfocusEdit1()
{
	UpdateData(1);
	maxW = minW + sStep*(count - 1);
	UpdateData(0);
}


void snp::OnEnKillfocusEdit5()
{
	UpdateData(1);
	maxW = minW + sStep*(count - 1);
	UpdateData(0);
}


void snp::OnEnKillfocusEdit2()
{
	UpdateData(1);
	maxP = minP + pStep*(count - 1);
	UpdateData(0);
}


void snp::OnEnKillfocusEdit6()
{
	UpdateData(1);
	maxP = minP + pStep*(count - 1);
	UpdateData(0);
}


void snp::OnEnChangeEdit7()
{
	UpdateData(1);
	if (count > 0)
	{
		GetDlgItem(IDOK)->EnableWindow(1);
		maxW = minW + sStep*(count - 1);
		maxP = minP + pStep*(count - 1);
	}
	else
	{
		MessageBox(_T("���������� ����������� ������ ���� ������ 0"));
		GetDlgItem(IDOK)->EnableWindow(0);
	}
	UpdateData(0);
}
