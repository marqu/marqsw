// scale.cpp: Realization file
//

#include "stdafx.h"
#include "imModificator_Scale.h"
#include "scale.h"
#include "afxdialogex.h"


// Dialog window scale

IMPLEMENT_DYNAMIC(scale, CDialog)

scale::scale(CWnd* pParent /*=NULL*/)
	: CDialog(scale::IDD, pParent)
	, mins(1)
	, mStep(0.5)
	, maxs(5.5)
	, count(10)
	, copies(1)
{

}

scale::~scale()
{
}

void scale::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, mins);
	DDX_Text(pDX, IDC_EDIT4, mStep);
	DDX_Text(pDX, IDC_EDIT2, maxs);
	DDX_Text(pDX, IDC_EDIT3, count);
	DDX_Text(pDX, IDC_EDIT8, copies);
}


BEGIN_MESSAGE_MAP(scale, CDialog)
	ON_BN_CLICKED(IDOK, &scale::OnBnClickedOk)
	ON_WM_DESTROY()
	ON_EN_KILLFOCUS(IDC_EDIT1, &scale::OnEnKillfocusEdit1)
	ON_EN_KILLFOCUS(IDC_EDIT4, &scale::OnEnKillfocusEdit4)
	ON_EN_CHANGE(IDC_EDIT3, &scale::OnEnChangeEdit3)
END_MESSAGE_MAP()


// Message handlers scale

BOOL scale::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (imPath)
	{
		readRefImage(imPath);
		CString buf;
		buf.Format(L"������� ������� �����������: %d x %d", refImg.rows, refImg.cols);
		SetDlgItemTextW(IDC_STATIC, buf);
	}
	else
		SetDlgItemTextW(IDC_STATIC, L"������������ ���� �����������");
	return TRUE;  // return TRUE unless you set the focus to a control
}

bool scale::readRefImage(CString path)
{
	int rpl = path.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(path);
	refImg = imread(lpANSI);
	if (refImg.data)
	{
		return true;
	}
	else return false;
}

double scale::getParameters(int i)
{
	return mStep;
}

Mat scale::process()
{
	Mat src, frame;
	src = refImg.clone();
	double newR = src.rows*param;
	double newC = src.cols*param;
	Size newSize = { (long)newR, (long)newC };
	resize(src, frame, newSize, 0, 0, CV_INTER_CUBIC);
	src.release();
	return frame;
}

bool scale::flashOutputImages(CString path, int num, int cpy)
{
	int rpl;
	CString buf;
	if (cpy > 1)
		buf.Format(_T("\\img_%d(%d).jpg"), num + 1, cpy);
	else
		buf.Format(_T("\\img_%d.jpg"), num + 1);
	CString newPath = path + buf;
	rpl = newPath.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(newPath);
	imwrite(lpANSI, processed);
	FILE* file;
	CString fileName = path + L"/Description.txt";
	rpl = fileName.Replace(_T('\\'), _T('/'));
	lpANSI = W2A(fileName);
	if ((num == 0) && (cpy == 1)) remove(lpANSI);
	fopen_s(&file, lpANSI, "a");
	if (file != 0)
	{
		if ((num == 0) && (cpy == 1))
		{
			CString mod;
			mod.Format(_T("Modificator: Scaling. %d\n"), this->copies);
			lpANSI = W2A(mod);
			fprintf(file, lpANSI);
		}
		CString buf;
		if (cpy > 1)
			buf.Format(_T("img_%d(%d).jpg\tScale_Coefficient: %.3f\n"), num + 1, cpy, param);
		else
			buf.Format(_T("img_%d.jpg\tScale_Coefficient: %.3f\n"), num + 1, param);
		lpANSI = W2A(buf);
		fprintf(file, lpANSI);
	}
	fclose(file);
	return true;
}

void scale::OnBnClickedOk()
{
	UpdateData(1);
	if (imPath)
	{
		param = mins;
		for (UINT i = 0; i < count; i++)
		{
			for (UINT c = 1; c < copies + 1; c++)
			{
				processed = process();
				flashOutputImages(savePath, i, c);
			}
			param *= getParameters(i);
		}
		MessageBox(_T("������� ���������"));
	}
	UpdateData(0);
}


void scale::OnDestroy()
{
	refImg.release();
	processed.release();
	CDialog::OnDestroy();
}




void scale::OnEnKillfocusEdit1()
{
	UpdateData(1);
	maxs = mins * mStep*(count - 1);
	UpdateData(0);
}


void scale::OnEnKillfocusEdit4()
{
	UpdateData(1);
	maxs = mins * mStep*(count - 1);
	UpdateData(0);
}


void scale::OnEnChangeEdit3()
{
	UpdateData(1);
	if (count > 0)
	{
		GetDlgItem(IDOK)->EnableWindow(1);
		maxs = mins + mStep*(count - 1);
	}
	else
	{
		MessageBox(_T("���������� ����������� ������ ���� ������ 0"));
		GetDlgItem(IDOK)->EnableWindow(0);
	}
	UpdateData(0);
}
