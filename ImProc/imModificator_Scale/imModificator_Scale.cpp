// imModificator_Scale.cpp: defines default procedures of initialization DLL.
//

#include "stdafx.h"
#include "imModificator_Scale.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CimModificator_ScaleApp

BEGIN_MESSAGE_MAP(CimModificator_ScaleApp, CWinApp)
END_MESSAGE_MAP()


// Creation CimModificator_ScaleApp

CimModificator_ScaleApp::CimModificator_ScaleApp()
{
}

CimModificator_ScaleApp theApp;

BOOL CimModificator_ScaleApp::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}

scale SC;

HRESULT __stdcall getName(CString* name)
{
	*name = _T("���������������");
	return S_OK;
}

HRESULT __stdcall setParams(CString refImgPath, CString savPath)
{
	SC.imPath = refImgPath;
	SC.savePath = savPath;
	return S_OK;
}

HRESULT __stdcall getDialog()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	SC.DoModal();
	return S_OK;
}