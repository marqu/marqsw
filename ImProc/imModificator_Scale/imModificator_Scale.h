// imModificator_Scale.h: main header file for DLL imModificator_Scale
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include stdafx.h before including this file in PCH"
#endif

#include "resource.h"		// basic symbols
#include "scale.h"

// CimModificator_ScaleApp
// For realization of this class see imModificator_Scale.cpp
//
extern "C" __declspec(dllexport) HRESULT __stdcall getName(CString* name);
extern "C" __declspec(dllexport) HRESULT __stdcall getDialog();
extern "C" __declspec(dllexport) HRESULT __stdcall setParams(CString refImgPath, CString savImgPath);

class CimModificator_ScaleApp : public CWinApp
{
public:
	CimModificator_ScaleApp();

// Redefinition
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
