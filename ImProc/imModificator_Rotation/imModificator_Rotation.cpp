// imModificator_Rotation.dll.cpp: defines default procedures of initialization DLL.
//

#include "stdafx.h"
#include "imModificator_Rotation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CimModificator_RotationdllApp

BEGIN_MESSAGE_MAP(CimModificator_RotationdllApp, CWinApp)
END_MESSAGE_MAP()


// Creation CimModificator_RotationdllApp

CimModificator_RotationdllApp::CimModificator_RotationdllApp()
{
}

CimModificator_RotationdllApp theApp;

BOOL CimModificator_RotationdllApp::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}

rt RT;

HRESULT __stdcall getName(CString* name)
{
	*name = _T("�������");
	return S_OK;
}

HRESULT __stdcall setParams(CString refImgPath, CString savPath)
{
	RT.imPath = refImgPath;
	RT.savePath = savPath;
	return S_OK;
}

HRESULT __stdcall getDialog()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	RT.DoModal();
	return S_OK;
}