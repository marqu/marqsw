#pragma once
#include "afxcmn.h"
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
using namespace cv;

/** Dialog window class rt
* @author	Egor Serov
*/
class rt : public CDialog
{
	DECLARE_DYNAMIC(rt)

public:
	rt(CWnd* pParent = NULL);   // Default constructor
	virtual ~rt();

	// Data of dialog window
	enum { IDD = IDD_RT };
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Support of DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	double minr;
	double maxr;
	Mat refImg;
	Mat processed;
	CString imPath;
	CString savePath;
	double param;
	double mStep;
	int count;
	int copies;

	/**
	* This method applies specific distortion with defined parameters to an image
	* @return modified image
	*/
	Mat process();

	/**
	* This method loads an image with specified full path
	* @return success/failure flag
	*/
	bool readRefImage(CString path);

	/**
	* This method returns parameters for current step of distortion cycle
	* @return parameters of rotation (rotation angle in degrees)
	*/
	double getParameters(int i);

	/**
	* This method saves modified images with description file
	* @param path - full path to directory where results should be saved
	* @param num - number of modified images
	* @param cpy - number of modified image copies with specific parameters of 
	*              distortion
	* @return success/failure flag
	*/
	bool flashOutputImages(CString path, int num, int cpy);
	afx_msg void OnBnClickedOk();
	afx_msg void OnEnKillfocusEdit1();
	afx_msg void OnEnKillfocusEdit4();
	afx_msg void OnEnChangeEdit3();
	afx_msg void OnDestroy();
};
