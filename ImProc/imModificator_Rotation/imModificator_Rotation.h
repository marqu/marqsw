// imModificator_Rotation.dll.h: main header file for DLL imModificator_Rotation.dll
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include stdafx.h before including this file in PCH"
#endif

#include "resource.h"		// basic symbols
#include "rt.h"

// CimModificator_RotationdllApp
// For realization of this class see imModificator_Rotation.dll.cpp
//
extern "C" __declspec(dllexport) HRESULT __stdcall getName(CString* name);
extern "C" __declspec(dllexport) HRESULT __stdcall getDialog();
extern "C" __declspec(dllexport) HRESULT __stdcall setParams(CString refImgPath, CString savImgPath);

class CimModificator_RotationdllApp : public CWinApp
{
public:
	CimModificator_RotationdllApp();

// Redefinition
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
