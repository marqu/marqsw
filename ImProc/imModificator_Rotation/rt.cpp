// rt.cpp: Realization file
//

#include "stdafx.h"
#include "imModificator_Rotation.h"
#include "rt.h"
#include "afxdialogex.h"
#include <math.h>

// Dialog window rt

IMPLEMENT_DYNAMIC(rt, CDialog)

rt::rt(CWnd* pParent /*=NULL*/)
	: CDialog(rt::IDD, pParent)
	, minr(0)
	, maxr(90)
	, count(10)
	, mStep(0)
	, copies(1)
{

}

rt::~rt()
{
}

void rt::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, minr);
	DDX_Text(pDX, IDC_EDIT2, maxr);
	DDX_Text(pDX, IDC_EDIT4, mStep);
	DDX_Text(pDX, IDC_EDIT3, count);
	DDX_Text(pDX, IDC_EDIT8, copies);
}


BEGIN_MESSAGE_MAP(rt, CDialog)
	ON_BN_CLICKED(IDOK, &rt::OnBnClickedOk)
	ON_WM_DESTROY()
	ON_EN_KILLFOCUS(IDC_EDIT1, &rt::OnEnKillfocusEdit1)
	ON_EN_KILLFOCUS(IDC_EDIT4, &rt::OnEnKillfocusEdit4)
	ON_EN_CHANGE(IDC_EDIT3, &rt::OnEnChangeEdit3)
END_MESSAGE_MAP()


// Message handlers rt

bool rt::readRefImage(CString path)
{
	int rpl = path.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(path);
	refImg = imread(lpANSI);
	if (refImg.data)
	{
		return true;
	}
	else return false;
}

double rt::getParameters(int i)
{
	if (i == 0)return 0.;
	else return mStep;
}

Mat rt::process()
{
	Mat src, frame, frameRotated;
	src = refImg;

	int diagonal = (int)sqrt(src.cols*src.cols + src.rows*src.rows);
	int newWidth = diagonal;
	int newHeight = diagonal;

	int offsetX = (newWidth - src.cols) / 2;
	int offsetY = (newHeight - src.rows) / 2;
	Mat targetMat(newWidth, newHeight, src.type());
	Point2f src_center(targetMat.cols / 2.0F, targetMat.rows / 2.0F);

	src.copyTo(frame);

	frame.copyTo(targetMat.rowRange(offsetY, offsetY + frame.rows).colRange(offsetX, offsetX + frame.cols));
	Mat rot_mat = getRotationMatrix2D(src_center, param, 1.0);
	warpAffine(targetMat, frameRotated, rot_mat, targetMat.size(), 1, BORDER_TRANSPARENT);

	Rect bound_Rect(frame.cols, frame.rows, 0, 0);

	int x1 = offsetX;
	int x2 = offsetX + frame.cols;
	int x3 = offsetX;
	int x4 = offsetX + frame.cols;

	int y1 = offsetY;
	int y2 = offsetY;
	int y3 = offsetY + frame.rows;
	int y4 = offsetY + frame.rows;

	Mat co_Ordinate = (Mat_<double>(3, 4) << x1, x2, x3, x4,
		y1, y2, y3, y4,
		1, 1, 1, 1);
	Mat RotCo_Ordinate = rot_mat * co_Ordinate;

	for (int i = 0; i < 4; i++){
		if (RotCo_Ordinate.at<double>(0, i) < bound_Rect.x)
			bound_Rect.x = (int)RotCo_Ordinate.at<double>(0, i); //access smallest 
		if (RotCo_Ordinate.at<double>(1, i)<bound_Rect.y)
			bound_Rect.y = RotCo_Ordinate.at<double>(1, i); //access smallest y
	}

	for (int i = 0; i<4; i++){
		if (RotCo_Ordinate.at<double>(0, i)>bound_Rect.width)
			bound_Rect.width = (int)RotCo_Ordinate.at<double>(0, i); //access largest x
		if (RotCo_Ordinate.at<double>(1, i)>bound_Rect.height)
			bound_Rect.height = RotCo_Ordinate.at<double>(1, i); //access largest y
	}

	bound_Rect.width = bound_Rect.width - bound_Rect.x;
	bound_Rect.height = bound_Rect.height - bound_Rect.y;

	Mat cropedResult;
	Mat ROI = frameRotated(bound_Rect);
	ROI.copyTo(cropedResult);
	ROI.release();
	src.release();
	frame.release();
	frameRotated.release();
	co_Ordinate.release();
	RotCo_Ordinate.release();
	rot_mat.release();
	return cropedResult;
}

bool rt::flashOutputImages(CString path, int num, int cpy)
{
	int rpl;
	CString buf;
	if (cpy > 1)
		buf.Format(_T("\\img_%d(%d).jpg"), num + 1, cpy);
	else
		buf.Format(_T("\\img_%d.jpg"), num + 1);
	CString newPath = path + buf;
	rpl = newPath.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(newPath);
	imwrite(lpANSI, processed);
	FILE* file;
	CString fileName = path + L"/Description.txt";
	rpl = fileName.Replace(_T('\\'), _T('/'));
	lpANSI = W2A(fileName);
	if ((num == 0) && (cpy == 1)) remove(lpANSI);
	fopen_s(&file, lpANSI, "a");
	if (file != 0)
	{
		if ((num == 0) && (cpy == 1))
		{
			CString mod;
			mod.Format(_T("Modificator: Rotation. %d\n"), this->copies);
			lpANSI = W2A(mod);
			fprintf(file, lpANSI);
		}
		CString buf;
		if (cpy > 1)
			buf.Format(_T("img_%d(%d).jpg\tAngle: %.2f\n"), num + 1, cpy, param);
		else
			buf.Format(_T("img_%d.jpg\tAngle: %.2f\n"), num + 1, param);
		lpANSI = W2A(buf);
		fprintf(file, lpANSI);
	}
	fclose(file);
	return true;
}
void rt::OnBnClickedOk()
{
	UpdateData(1);
	if (imPath)
	{
		readRefImage(imPath);
		param = minr;
		for (UINT i = 0; i < count; i++)
		{
			param += getParameters(i);
			for (UINT c = 1; c < copies + 1; c++)
			{
				processed = process();
				flashOutputImages(savePath, i, c);
			}
		}
		MessageBox(_T("������� ���������"));
	}
	UpdateData(0);
}


void rt::OnDestroy()
{
	refImg.release();
	processed.release();
	CDialog::OnDestroy();
}


void rt::OnEnKillfocusEdit1()
{
	UpdateData(1);
	maxr = minr + mStep*(count - 1);
	UpdateData(0);
}


void rt::OnEnKillfocusEdit4()
{
	UpdateData(1);
	maxr = minr + mStep*(count - 1);
	UpdateData(0);
}


void rt::OnEnChangeEdit3()
{
	UpdateData(1);
	if (count > 0)
	{
		GetDlgItem(IDOK)->EnableWindow(1);
		maxr = minr + mStep*(count - 1);
	}
	else
	{
		MessageBox(_T("���������� ����������� ������ ���� ������ 0"));
		GetDlgItem(IDOK)->EnableWindow(0);
	}
	UpdateData(0);
}
