#pragma once

#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"

using namespace cv;

/** Dialog window class prDist
* @author	Egor Serov
*/
class prDist : public CDialogEx
{
	DECLARE_DYNAMIC(prDist)
public:
	prDist(CWnd* pParent = NULL);   // Default constructor
	virtual ~prDist();

	// Data of dialog window
	enum { IDD = IDD_PRDIST };
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Support of DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	double mind;
	double dStep;
	double maxd;
	int count;
	int copies;
	Mat refImg;
	Mat processed;
	CString imPath;
	CString savePath;
	double param;

	/**
	* This method applies specific distortion with defined parameters to an image
	* @return modified image
	*/
	Mat process();

	/**
	* This method loads an image with specified full path
	* @return success/failure flag
	*/
	bool readRefImage(CString path);

	/**
	* This method returns parameters for current step of distortion cycle
	* @return parameters of rotation (rotation angle in degrees)
	*/
	double getParameters(int i);

	/**
	* This method saves modified images with description file
	* @param path - full path to directory where results should be saved
	* @param num - number of modified images
	* @param cpy - number of modified image copies with specific parameters of 
	*              distortion
	* @return success/failure flag
	*/
	bool flashOutputImages(CString path, int num, int cpy);
	afx_msg void OnBnClickedOk();
};
