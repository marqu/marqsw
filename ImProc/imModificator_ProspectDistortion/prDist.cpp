// prDist.cpp: Realization file
//

#include "stdafx.h"
#include "imModificator_ProspectDistortion.h"
#include "prDist.h"
#include "afxdialogex.h"


// Dialog window prDist

IMPLEMENT_DYNAMIC(prDist, CDialogEx)

prDist::prDist(CWnd* pParent /*=NULL*/)
	: CDialogEx(prDist::IDD, pParent)
	, mind(90)
	, maxd(0)
	, count(10)
	, dStep(9)
	, copies(1)
{

}

prDist::~prDist()
{
}

void prDist::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(prDist, CDialogEx)
	ON_BN_CLICKED(IDOK, &prDist::OnBnClickedOk)
END_MESSAGE_MAP()


// Message handlers prDist
bool prDist::readRefImage(CString path)
{
	int rpl = path.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(path);
	refImg = imread(lpANSI);
	if (refImg.data)
	{
		return true;
	}
	else return false;
}

// This function was taken from 
// http://jepsonsblog.blogspot.ru/2012/11/rotation-in-3d-using-opencvs.html
void rotateImage(const Mat &input, Mat &output, double alpha, double beta, double gamma, 
				 double dx, double dy, double dz, double f)
{
	alpha = (alpha - 90.)*CV_PI / 180.;
	beta = (beta - 90.)*CV_PI / 180.;
	gamma = (gamma - 90.)*CV_PI / 180.;

	// get width and height for ease of use in matrices
	double w = (double)input.cols;
	double h = (double)input.rows;

	// Projection 2D -> 3D matrix
	Mat A1 = (Mat_<double>(4, 3) <<
		1, 0, -w / 2,
		0, 1, -h / 2,
		0, 0, 0,
		0, 0, 1);

	// Rotation matrices around the X, Y, and Z axis
	Mat RX = (Mat_<double>(4, 4) <<
		1, 0, 0, 0,
		0, cos(alpha), -sin(alpha), 0,
		0, sin(alpha), cos(alpha), 0,
		0, 0, 0, 1);

	Mat RY = (Mat_<double>(4, 4) <<
		cos(beta), 0, -sin(beta), 0,
		0, 1, 0, 0,
		sin(beta), 0, cos(beta), 0,
		0, 0, 0, 1);

	Mat RZ = (Mat_<double>(4, 4) <<
		cos(gamma), -sin(gamma), 0, 0,
		sin(gamma), cos(gamma), 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1);

	// Composed rotation matrix with (RX, RY, RZ)
	Mat R = RX * RY * RZ;

	// Translation matrix
	Mat T = (Mat_<double>(4, 4) <<
		1, 0, 0, dx,
		0, 1, 0, dy,
		0, 0, 1, dz,
		0, 0, 0, 1);

	// 3D -> 2D matrix
	Mat A2 = (Mat_<double>(3, 4) <<
		f, 0, w / 2, 0,
		0, f, h / 2, 0,
		0, 0, 1, 0);

	// Final transformation matrix
	Mat trans = A2 * (T * (R * A1));

	// Apply matrix transformation
	warpPerspective(input, output, trans, input.size(), INTER_LANCZOS4);
}

double prDist::getParameters(int i)
{
	return mind + i*dStep;
}

Mat prDist::process()
{
	Mat tmp = refImg.clone();
	rotateImage(refImg, tmp, 90, param, 90, 0, 0, tmp.cols, tmp.cols);
	return tmp;
}

bool prDist::flashOutputImages(CString path, int num, int cpy)
{
	int rpl;
	CString buf;
	if (cpy > 1)
		buf.Format(_T("\\img_%d(%d).jpg"), num + 1, cpy);
	else
		buf.Format(_T("\\img_%d.jpg"), num + 1);
	CString newPath = path + buf;
	rpl = newPath.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(newPath);
	imwrite(lpANSI, processed);
	FILE* file;
	CString fileName = path + L"/Description.txt";
	rpl = fileName.Replace(_T('\\'), _T('/'));
	lpANSI = W2A(fileName);
	if ((num == 0) && (cpy == 1)) remove(lpANSI);
	fopen_s(&file, lpANSI, "a");
	if (file != 0)
	{
		if ((num == 0) && (cpy == 1))
		{
			CString mod;
			mod.Format(_T("Modificator: Prospect distortion. %d\n"), this->copies);
			lpANSI = W2A(mod);
			fprintf(file, lpANSI);
		}
	}
	fclose(file);
	return true;
}

void prDist::OnBnClickedOk()
{
	UpdateData(1);
	if (imPath)
	{
		readRefImage(imPath);
		param = mind;
		for (UINT i = 0; i < count; i++)
		{
			param = getParameters(i);
			for (UINT c = 1; c < copies + 1; c++)
			{
				processed = process();
				flashOutputImages(savePath, i, c);
			}
		}
		MessageBox(_T("������� ���������"));
	}
	UpdateData(0);
}
