// imModificator_ProspectDistortion.cpp: defines default procedures of initialization DLL.
//

#include "stdafx.h"
#include "imModificator_ProspectDistortion.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CimModificator_ProspectDistortionApp

BEGIN_MESSAGE_MAP(CimModificator_ProspectDistortionApp, CWinApp)
END_MESSAGE_MAP()

// Creation CimModificator_ProspectDistortionApp

CimModificator_ProspectDistortionApp::CimModificator_ProspectDistortionApp()
{
}

CimModificator_ProspectDistortionApp theApp;

BOOL CimModificator_ProspectDistortionApp::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}

prDist pd;

HRESULT __stdcall getName(CString* name)
{
	*name = _T("��������� �����������");
	return S_OK;
}

HRESULT __stdcall setParams(CString refImgPath, CString savPath)
{
	pd.imPath = refImgPath;
	pd.savePath = savPath;
	return S_OK;
}

HRESULT __stdcall getDialog()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	pd.DoModal();
	return S_OK;
}