// imModificator_ProspectDistortion.h: main header file for DLL imModificator_ProspectDistortion
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include stdafx.h before including this file in PCH"
#endif

#include "resource.h"		// basic symbols
#include "prDist.h"

// CimModificator_ProspectDistortionApp
// For realization of this class see imModificator_ProspectDistortion.cpp
//
extern "C" __declspec(dllexport) HRESULT __stdcall getName(CString* name);
extern "C" __declspec(dllexport) HRESULT __stdcall getDialog();
extern "C" __declspec(dllexport) HRESULT __stdcall setParams(CString refImgPath, CString savImgPath);

class CimModificator_ProspectDistortionApp : public CWinApp
{
public:
	CimModificator_ProspectDistortionApp();

// Redefinition
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
