// imModificator_GaussianNoise.h: main header file for DLL imModificator_GaussianNoise
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include stdafx.h before including this file in PCH"
#endif

#include "resource.h"		// basic symbols
#include "gn.h"

// CimModificator_GaussianNoiseApp
// For realization of this class see imModificator_GaussianNoise.cpp
//
extern "C" __declspec(dllexport) HRESULT __stdcall getName(CString* name);
extern "C" __declspec(dllexport) HRESULT __stdcall getDialog();
extern "C" __declspec(dllexport) HRESULT __stdcall setParams(CString refImgPath, CString savImgPath);

class CimModificator_GaussianNoiseApp : public CWinApp
{
public:
	CimModificator_GaussianNoiseApp();

// Redefinition
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
