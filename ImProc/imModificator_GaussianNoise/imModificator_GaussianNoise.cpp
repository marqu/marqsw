// imModificator_GaussianNoise.cpp: defines default procedures of initialization DLL.
//

#include "stdafx.h"
#include "imModificator_GaussianNoise.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CimModificator_GaussianNoiseApp

BEGIN_MESSAGE_MAP(CimModificator_GaussianNoiseApp, CWinApp)
END_MESSAGE_MAP()


// Creation CimModificator_GaussianNoiseApp

CimModificator_GaussianNoiseApp::CimModificator_GaussianNoiseApp()
{
}

CimModificator_GaussianNoiseApp theApp;

BOOL CimModificator_GaussianNoiseApp::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}

gn GN;

HRESULT __stdcall getName(CString* name)
{
	*name = _T("������� ���");
	return S_OK;
}

HRESULT __stdcall setParams(CString refImgPath, CString savPath)
{
	GN.imPath = refImgPath;
	GN.savePath = savPath;
	return S_OK;
}

HRESULT __stdcall getDialog()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	GN.DoModal();
	return S_OK;
}