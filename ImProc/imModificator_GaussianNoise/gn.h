#pragma once
#include "afxcmn.h"

#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
using namespace cv;
#define _CRTDBG_MAPALLOC

struct Noise
{
	double mean;
	double deviat;
};

/** Dialog window class gn
* @author	Egor Serov
*/
class gn : public CDialog
{
	DECLARE_DYNAMIC(gn)

public:
	gn(CWnd* pParent = NULL);   // Default constructor
	virtual ~gn();

	// Data of dialog window
	enum { IDD = IDD_GN };
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Support of DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	double mind;
	double maxd;
	double minmean;
	double maxmean;
	Mat refImg;
	Mat processed;
	CString imPath;
	CString savePath;
	Noise param;
	double dStep;
	double mStep;
	int count;
	int copies;

	/**
	* This method applies specific distortion with defined parameters to an image
	* @return modified image
	*/
	Mat process();

	/**
	* This method loads an image with specified full path
	* @return success/failure flag
	*/
	bool readRefImage(CString path);

	/**
	* This method returns parameters for current step of distortion cycle
	* @return parameters of Gaussian noise (mean and deviation)
	*/
	Noise getParameters(int i);

	/**
	* This method saves modified images with description file
	* @param path - full path to directory where results should be saved
	* @param num - number of modified images
	* @param cpy - number of modified image copies with specific parameters of 
	*              distortion
	* @return success/failure flag
	*/
	bool flashOutputImages(CString path, int num, int cpy);
	afx_msg void OnBnClickedOk();
	afx_msg void OnEnChangeEdit1();
	afx_msg void OnEnKillfocusMind();
	afx_msg void OnEnKillfocusMind2();
	afx_msg void OnEnKillfocusMinmean();
	afx_msg void OnEnKillfocusMinmean2();
	afx_msg void OnDestroy();
};
