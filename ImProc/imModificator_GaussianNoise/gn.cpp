// gn.cpp: Realization file
//

#include "stdafx.h"
#include "imModificator_GaussianNoise.h"
#include "gn.h"
#include "afxdialogex.h"


// Dialog window gn

IMPLEMENT_DYNAMIC(gn, CDialog)

gn::gn(CWnd* pParent /*=NULL*/)
	: CDialog(gn::IDD, pParent)
	, mind(1)
	, maxd(91.0)
	, minmean(0)
	, maxmean(90.0)
	, count(10)
	, dStep(10)
	, mStep(10)
	, copies(1)
{

}

gn::~gn()
{
}

void gn::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_MIND, mind);
	DDX_Text(pDX, IDC_MAXD, maxd);
	DDX_Text(pDX, IDC_MINMEAN, minmean);
	DDX_Text(pDX, IDC_MAXMEAN, maxmean);
	DDX_Text(pDX, IDC_MIND2, dStep);
	DDX_Text(pDX, IDC_MINMEAN2, mStep);
	DDX_Text(pDX, IDC_EDIT1, count);
	DDX_Text(pDX, IDC_EDIT2, copies);
}


BEGIN_MESSAGE_MAP(gn, CDialog)
	ON_BN_CLICKED(IDOK, &gn::OnBnClickedOk)
	ON_WM_DESTROY()
	ON_EN_CHANGE(IDC_EDIT1, &gn::OnEnChangeEdit1)
	ON_EN_KILLFOCUS(IDC_MIND, &gn::OnEnKillfocusMind)
	ON_EN_KILLFOCUS(IDC_MIND2, &gn::OnEnKillfocusMind2)
	ON_EN_KILLFOCUS(IDC_MINMEAN, &gn::OnEnKillfocusMinmean)
	ON_EN_KILLFOCUS(IDC_MINMEAN2, &gn::OnEnKillfocusMinmean2)
END_MESSAGE_MAP()


// Message handlers gn
bool gn::readRefImage(CString path)
{
	int rpl = path.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(path);
	refImg = imread(lpANSI);
	if (refImg.data)
	{
		return true;
	}
	else return false;
}

Noise gn::getParameters(int i)
{
	Noise params;
	params.mean = minmean + mStep*i;
	params.deviat = mind + dStep*i;
	return params;
}

Mat gn::process()
{
	Mat tmp = refImg;
	cvtColor(tmp, tmp, 7);
	tmp.convertTo(tmp, CV_32FC1);
	Mat noise = tmp.clone();
	noise.setTo(0);
	randn(noise, param.mean, param.deviat);
	tmp += noise;
	noise.release();
	return tmp;
}

bool gn::flashOutputImages(CString path, int num, int cpy)
{
	int rpl;
	CString buf;
	if (cpy > 1)
		buf.Format(_T("\\img_%d(%d).jpg"), num + 1, cpy);
	else
		buf.Format(_T("\\img_%d.jpg"), num + 1);
	CString newPath = path + buf;
	rpl = newPath.Replace(_T('\\'), _T('/'));
	double*ha = new double[100];
	USES_CONVERSION;
	LPSTR lpANSI = W2A(newPath);
	imwrite(lpANSI, processed);
	FILE* file;
	CString fileName = path + L"/Description.txt";
	rpl = fileName.Replace(_T('\\'), _T('/'));
	lpANSI = W2A(fileName);
	if ((num == 0) && (cpy == 1)) remove(lpANSI);
	fopen_s(&file, lpANSI, "a");
	if (file != 0)
	{
		if ((num == 0) && (cpy == 1))
		{
			CString mod;
			mod.Format(_T("Modificator: Gaussian Noise.  %d\n"), this->copies);
			lpANSI = W2A(mod);
			fprintf(file, lpANSI);
			mod.ReleaseBuffer();
		}
		CString buf;
		if (cpy > 1)
			buf.Format(_T("img_%d(%d).jpg\tMean: %.2f\tDeviation: %.2f\n"), 
					   num + 1, cpy, param.mean, param.deviat);
		else
			buf.Format(_T("img_%d.jpg\tMean: %.2f\tDeviation: %.2f\n"), 
					   num + 1, param.mean, param.deviat);
		lpANSI = W2A(buf);
		fprintf(file, lpANSI);
		buf.ReleaseBuffer();
	}
	fclose(file);
	buf.ReleaseBuffer();
	newPath.ReleaseBuffer();
	fileName.ReleaseBuffer();
	return true;
}

void gn::OnBnClickedOk()
{
	UpdateData(1);
	if (imPath)
	{
		if ((mind > 0) && (maxd >= 0))
		{
			readRefImage(imPath);
			for (UINT i = 0; i < count; i++)
			{
				param = getParameters(i);
				for (UINT c = 1; c < copies + 1; c++)
				{
					processed = process();
					flashOutputImages(savePath, i, c);
				}
			}
			MessageBox(_T("������� ���������"));
		}
		else MessageBox(_T("�� ���������! ��������� ���������:\n��������� ������ ���� ������ 0\n���������� ����������� ������ ���� ������ 0"));
	}
	UpdateData(0);
}

void gn::OnDestroy()
{
	refImg.release();
	processed.release();
	CDialog::OnDestroy();
}


void gn::OnEnKillfocusMind()
{
	UpdateData(1);
	maxd = mind + dStep*(count - 1);
	UpdateData(0);
}

void gn::OnEnKillfocusMind2()
{
	UpdateData(1);
	maxd = mind + dStep*(count - 1);
	UpdateData(0);
}


void gn::OnEnKillfocusMinmean()
{
	UpdateData(1);
	maxmean = minmean + mStep*(count - 1);
	UpdateData(0);
}


void gn::OnEnKillfocusMinmean2()
{
	UpdateData(1);
	maxmean = minmean + mStep*(count - 1);
	UpdateData(0);
}

void gn::OnEnChangeEdit1()
{
	UpdateData(1);
	if (count > 0)
	{
		GetDlgItem(IDOK)->EnableWindow(1);
		maxd = mind + dStep*(count - 1);
		maxmean = minmean + mStep*(count - 1);
	}
	else
	{
		MessageBox(_T("���������� ����������� ������ ���� ������ 0"));
		GetDlgItem(IDOK)->EnableWindow(0);
	}
	UpdateData(0);
}