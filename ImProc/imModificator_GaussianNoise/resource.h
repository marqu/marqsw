//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется imModificator_GaussianNoise.rc
//
#define IDD_GN                          7000
#define IDC_MIND                        7000
#define IDC_MAXD                        7001
#define IDC_MINMEAN                     7002
#define IDC_MAXMEAN                     7003
#define IDC_MIND2                       7004
#define IDC_MINMEAN2                    7005
#define IDC_EDIT1                       7006
#define IDC_EDIT2                       7007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        7000
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         7008
#define _APS_NEXT_SYMED_VALUE           7001
#endif
#endif
