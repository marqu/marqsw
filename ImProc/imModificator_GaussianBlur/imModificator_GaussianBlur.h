// imModificator_GaussianBlur.h: main header file for DLL imModificator_GaussianBlur
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include stdafx.h before including this file in PCH"
#endif

#include "resource.h"		// basic symbols
#include "GaussBlur.h"

// CimModificator_GaussianBlurApp
// For realization of this class see imModificator_GaussianBlur.cpp
//

//////////////////////////////////////////////////////////////////////////
// Exported function of creation an object of component

extern "C" __declspec(dllexport) HRESULT __stdcall getName(CString* name);
extern "C" __declspec(dllexport) HRESULT __stdcall getDialog();
extern "C" __declspec(dllexport) HRESULT __stdcall setParams(CString refImgPath, CString savImgPath);

class CimModificator_GaussianBlurApp : public CWinApp
{
public:
	CimModificator_GaussianBlurApp();

// Redefinition
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};