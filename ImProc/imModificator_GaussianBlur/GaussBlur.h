#pragma once
#include "afxcmn.h"
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
using namespace cv;

struct GaussParams
{
	int win;
	double disp;
};

/** Dialog window class GaussBlur
* @author	Egor Serov
*/
class GaussBlur : public CDialog
{
	DECLARE_DYNAMIC(GaussBlur)
public:
	GaussBlur(CWnd* pParent = NULL);   // Default constructor
	virtual ~GaussBlur();

	// Data of dialog window
	enum { IDD = IDD_GAUSSBLUR };
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Support of DDX/DDV
	DECLARE_MESSAGE_MAP()
public:
	int minWin;
	double maxWin;
	double minD;
	double maxD;
	Mat refImg;
	GaussParams param;
	Mat processed;
	CString imPath;
	CString savePath;
	int count;
	double wStep;
	double dStep;
	int copies;

	/**
	* This method applies specific distortion with defined parameters to an image
	* @return modified image
	*/
	Mat process();

	/**
	* This method loads an image with specified full path
	* @return success/failure flag
	*/
	bool readRefImage(CString path);

	/**
	* This method returns parameters for current step of distortion cycle
	* @return parameters of Gaussian blur (window size and dispersion)
	*/
	GaussParams getParameters(int i);

	/**
	* This method saves modified images with description file
	* @param path - full path to directory where results should be saved
	* @param num - number of modified images
	* @param cpy - number of modified image copies with specific parameters of 
	*              distortion
	* @return success/failure flag
	*/
	bool flashOutputImages(CString path, int num, int cpy);
	afx_msg void OnBnClickedOk();
	afx_msg void OnEnKillfocusEdit1();
	afx_msg void OnEnKillfocusEdit5();
	afx_msg void OnEnKillfocusEdit2();
	afx_msg void OnEnKillfocusEdit6();
	afx_msg void OnEnChangeEdit7();
};
