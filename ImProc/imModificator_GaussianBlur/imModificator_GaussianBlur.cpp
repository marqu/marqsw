// imModificator_GaussianBlur.cpp: defines default procedures of initialization DLL.
//

#include "stdafx.h"
#include "imModificator_GaussianBlur.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CimModificator_GaussianBlurApp

BEGIN_MESSAGE_MAP(CimModificator_GaussianBlurApp, CWinApp)
END_MESSAGE_MAP()

GaussBlur GB;

// Creation CimModificator_GaussianBlurApp

CimModificator_GaussianBlurApp::CimModificator_GaussianBlurApp()
{
}

CimModificator_GaussianBlurApp theApp;

BOOL CimModificator_GaussianBlurApp::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}

HRESULT __stdcall getName(CString* name)
{
	*name = _T("�������� �� ������");
	return S_OK;
}

HRESULT __stdcall setParams(CString refImgPath, CString savPath)
{
	GB.imPath = refImgPath;
	GB.savePath = savPath;
	return S_OK;
}

HRESULT __stdcall getDialog()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	GB.DoModal();
	return S_OK;
}