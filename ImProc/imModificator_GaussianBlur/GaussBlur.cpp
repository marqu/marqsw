// GaussBlur.cpp: Realization file
//

#include "stdafx.h"
#include "imModificator_GaussianBlur.h"
#include "GaussBlur.h"
#include "afxdialogex.h"


// Dialog window GaussBlur

IMPLEMENT_DYNAMIC(GaussBlur, CDialog)

GaussBlur::GaussBlur(CWnd* pParent /*=NULL*/)
: CDialog(GaussBlur::IDD, pParent)
, minWin(0)
, maxWin(9)
, minD(1)
, maxD(82)
, count(10)
, wStep(1)
, dStep(9)
, copies(1)
{

}

GaussBlur::~GaussBlur()
{
}

void GaussBlur::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, minWin);
	DDX_Text(pDX, IDC_EDIT3, maxWin);
	DDX_Text(pDX, IDC_EDIT2, minD);
	DDX_Text(pDX, IDC_EDIT4, maxD);
	DDX_Text(pDX, IDC_EDIT7, count);
	DDX_Text(pDX, IDC_EDIT5, wStep);
	DDX_Text(pDX, IDC_EDIT6, dStep);
	DDX_Text(pDX, IDC_EDIT8, copies);
}


BEGIN_MESSAGE_MAP(GaussBlur, CDialog)
	ON_BN_CLICKED(IDOK, &GaussBlur::OnBnClickedOk)
	ON_EN_KILLFOCUS(IDC_EDIT1, &GaussBlur::OnEnKillfocusEdit1)
	ON_EN_KILLFOCUS(IDC_EDIT5, &GaussBlur::OnEnKillfocusEdit5)
	ON_EN_KILLFOCUS(IDC_EDIT2, &GaussBlur::OnEnKillfocusEdit2)
	ON_EN_KILLFOCUS(IDC_EDIT6, &GaussBlur::OnEnKillfocusEdit6)
	ON_EN_CHANGE(IDC_EDIT7, &GaussBlur::OnEnChangeEdit7)
END_MESSAGE_MAP()


// Message handlers GaussBlur
bool GaussBlur::readRefImage(CString path)
{
	int rpl = path.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(path);
	refImg = imread(lpANSI);
	if (refImg.data)
	{
		return true;
	}
	else return false;
}

GaussParams GaussBlur::getParameters(int i)
{
	GaussParams params;
	params.win = minWin + wStep*i;
	params.disp = minD + dStep*i;
	return params;
}

Mat GaussBlur::process()
{
	Mat tmp = refImg.clone();
	if (param.win % 2 == NULL)
	{
		GaussianBlur(tmp, tmp, Size(param.win + 1, param.win + 1), param.disp, param.disp);
	}
	else
	{
		GaussianBlur(tmp, tmp, Size(param.win, param.win), param.disp, param.disp);
	}
	return tmp;
}

bool GaussBlur::flashOutputImages(CString path, int num, int cpy)
{
	int rpl;
	CString buf;
	if (cpy > 1)
		buf.Format(_T("\\img_%d(%d).jpg"), num + 1, cpy);
	else
		buf.Format(_T("\\img_%d.jpg"), num + 1);
	CString newPath = path + buf;
	rpl = newPath.Replace(_T('\\'), _T('/'));
	USES_CONVERSION;
	LPSTR lpANSI = W2A(newPath);
	imwrite(lpANSI, processed);
	FILE* file;
	CString fileName = path + L"/Description.txt";
	rpl = fileName.Replace(_T('\\'), _T('/'));
	lpANSI = W2A(fileName);
	if ((num == 0) && (cpy == 1)) remove(lpANSI);
	fopen_s(&file, lpANSI, "a");
	if (file != 0)
	{
		if ((num == 0) && (cpy == 1))
		{
			CString mod;
			mod.Format(_T("Modificator: Gaussian Blur.  %d\n"), this->copies);
			lpANSI = W2A(mod);
			fprintf(file, lpANSI);
		}
		CString buf;
		if (param.win % 2 == NULL)
		{
			if (cpy > 1)
				buf.Format(_T("img_%d(%d).jpg\tWindow: %d\tDeviation: %.2f\n"), 
						   num + 1, cpy, param.win + 1, param.disp);
			else
				buf.Format(_T("img_%d.jpg\tWindow: %d\tDeviation: %.2f\n"), 
						   num + 1, param.win + 1, param.disp);
		}
		else
		{
			if (cpy > 1)
				buf.Format(_T("img_%d(%d).jpg\tWindow: %d\tDeviation: %.2f\n"), 
						   num + 1, cpy, param.win, param.disp);
			else
				buf.Format(_T("img_%d.jpg\tWindow: %d\tDeviation: %.2f\n"), 
						   num + 1, param.win, param.disp);
		}
		lpANSI = W2A(buf);
		fprintf(file, lpANSI);
	}
	fclose(file);
	return true;
}

void GaussBlur::OnBnClickedOk()
{
	UpdateData(1);
	if (imPath)
	{
		if ((minD >= 0) && (maxD >= 0) && (minWin >= 0) && (maxWin >= 0))
		{
			readRefImage(imPath);
			for (UINT i = 0; i < count; i++)
			{
				param.disp = 0;
				param = getParameters(i);
				for (UINT c = 1; c < copies + 1; c++)
				{
					processed = process();
					flashOutputImages(savePath, i, c);
				}
			}
			MessageBox(_T("������� ���������"));
		}
		else MessageBox(_T("�� ���������! ��������� ���������:\n��������� ������ ���� ������ 0\n������ ���� ������ ���� ������ 0\n���������� ����������� ������ ���� ������ 0"));
	}
	UpdateData(0);
}

void GaussBlur::OnEnKillfocusEdit1()
{
	UpdateData(1);
	maxWin = minWin + wStep*(count - 1);
	UpdateData(0);
}


void GaussBlur::OnEnKillfocusEdit5()
{
	UpdateData(1);
	maxWin = minWin + wStep*(count - 1);
	UpdateData(0);
}


void GaussBlur::OnEnKillfocusEdit2()
{
	UpdateData(1);
	maxD = minD + dStep*(count - 1);
	UpdateData(0);
}


void GaussBlur::OnEnKillfocusEdit6()
{
	UpdateData(1);
	maxD = minD + dStep*(count - 1);
	UpdateData(0);
}


void GaussBlur::OnEnChangeEdit7()
{
	UpdateData(1);
	if (count > 0)
	{
		GetDlgItem(IDOK)->EnableWindow(1);
		maxWin = minWin + wStep*(count - 1);
		maxD = minD + dStep*(count - 1);
	}
	else
	{
		MessageBox(_T("���������� ����������� ������ ���� ������ 0"));
		GetDlgItem(IDOK)->EnableWindow(0);
	}
	UpdateData(0);
}