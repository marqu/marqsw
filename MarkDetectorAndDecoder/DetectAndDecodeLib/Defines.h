# pragma once

/*Bits position on original marker image*/
#define BIT_1_POS Point2f(381,361)
#define BIT_2_POS Point2f(349,334)
#define BIT_3_POS Point2f(318,308)
#define BIT_4_POS Point2f(409,200)
#define BIT_5_POS Point2f(370,214)
#define BIT_6_POS Point2f(332,228)
#define BIT_7_POS Point2f(285,96)
#define BIT_8_POS Point2f(277,136)
#define BIT_9_POS Point2f(270,177)
#define BIT_10_POS Point2f(131,151)
#define BIT_11_POS Point2f(163,177)
#define BIT_12_POS Point2f(194,204)
#define BIT_13_POS Point2f(103,312)
#define BIT_14_POS Point2f(142,298)
#define BIT_15_POS Point2f(180,284)
#define BIT_16_POS Point2f(228,414)
#define BIT_17_POS Point2f(235,376)
#define BIT_18_POS Point2f(242,335)
#define BIT_20_POS Point2f(256,295)
#define BIT_21_POS Point2f(219,267)
#define BIT_22_POS Point2f(233,224)
#define BIT_23_POS Point2f(279,224)
#define BIT_24_POS Point2f(293,267)
/*************************************/

/*Keypoints on original marker image*/
#define OR_SMALL_1_CIRCLE_CENTER Point2f(165,363)
#define OR_SMALL_2_CIRCLE_CENTER Point2f(118,231)
#define OR_SMALL_3_CIRCLE_CENTER Point2f(208,124)
#define OR_SMALL_4_CIRCLE_CENTER Point2f(346,148)
#define OR_SMALL_5_CIRCLE_CENTER Point2f(392,279)

#define OR_CENTER_AND_BIT_19_COORD Point2f(256,256)

#define OR_LEAF_1_RIGHT Point2f(376,397)
#define OR_LEAF_1_LEFT Point2f(416,350)
#define OR_LEAF_2_RIGHT Point2f(438,222)
#define OR_LEAF_2_LEFT Point2f(418,164)
#define OR_LEAF_3_RIGHT Point2f(318,81)
#define OR_LEAF_3_LEFT Point2f(258,71)
#define OR_LEAF_4_RIGHT Point2f(136,115)
#define OR_LEAF_4_LEFT Point2f(97,162)
#define OR_LEAF_5_RIGHT Point2f(73,290)
#define OR_LEAF_5_LEFT Point2f(95,347)
#define OR_LEAF_6_RIGHT Point2f(194,431)
#define OR_LEAF_6_LEFT Point2f(255,441)

#define OR_CENTER_0_WITHOUT_CIRCLE Point2f(278,316)
#define OR_CENTER_1 Point2f(318,268)
#define OR_CENTER_2 Point2f(296,207)
#define OR_CENTER_3 Point2f(235,197)
#define OR_CENTER_4 Point2f(193,245)
#define OR_CENTER_5 Point2f(216,304)

/*************************************/

#define KEYPOINT_COUNT 24

/*Keypoints indexes*/
#define SMALL_1_CIRCLE_CENTER 0
#define SMALL_2_CIRCLE_CENTER 1
#define SMALL_3_CIRCLE_CENTER 2
#define SMALL_4_CIRCLE_CENTER 3
#define SMALL_5_CIRCLE_CENTER 4

#define CENTER_AND_BIT_19_COORD 5

#define LEAF_1_RIGHT 23
#define LEAF_1_LEFT 22
#define LEAF_2_RIGHT 20
#define LEAF_2_LEFT 19
#define LEAF_3_RIGHT 17
#define LEAF_3_LEFT 16
#define LEAF_4_RIGHT 14
#define LEAF_4_LEFT 13
#define LEAF_5_RIGHT 11
#define LEAF_5_LEFT 10
#define LEAF_6_RIGHT 8
#define LEAF_6_LEFT 7

#define CENTER_0_WITHOUT_CIRCLE 6
#define CENTER_1 21
#define CENTER_2 18
#define CENTER_3 15
#define CENTER_4 12
#define CENTER_5 9
/************************************/