// DetectAndDecodeLib.h
/**
* @author Egor Serov
*/

#pragma once

#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\features2d\features2d.hpp"
#include "opencv2\calib3d\calib3d.hpp"
#include <cliext\vector>
#include <cliext\map>
#include "Defines.h"

using namespace System;
using namespace cv;

typedef std::pair<std::string, cv::Mat> imgPair;

namespace DetectAndDecodeLib {

	/**
	* Structure with used in algorithm parameters
	*/
	public value struct Parameters
	{
		int retinaKern;
		double tvdLambda;
		int tvdNiters;
		double umSigma;
		double umThreshold;
		double umAmount;
		int medianKernelSize;
		int thresholdBlockSize;
		int thresholdC;
	};

	/**
	* Class to detect and analyze countours
	*/
	private class ContourDetector
	{
	private:
		vector<vector<cv::Point> > contours;
		vector<vector<cv::Point> > fContours;
		vector<vector<cv::Point> > _points;
		vector<Vec4i> hierarchy;
	public:
		/**
		* Method detects all contours at image
		* @param src - image where contours should be detected
		*/
		void getAllContours(Mat src);

		/**
		* Method detects and analyzes contours at image
		*/
		void getFilteredContours();

		/**
		* Method draws detected contours at image
		* @param src - image where contours should be detected
		* @param dst - image where contours should be drawn
		* @param Contours - vector of contours that are drawn
		* @param contFiltration - if true, only filtered contours will be drawn and returned
		*/
		void getImWithContours(Mat src, Mat& dst, vector<vector<cv::Point>>& Contours,
			bool contFiltration);

		/**
		* Method returns detected keypoints
		* @param points - vector of detected points
		*/
		void getKeyPoints(vector<vector<cv::Point> >& points);
	};

	/**
	* Class to decode information from marker
	*/
	private class Decoder
	{
	public:
		void setDetectedMarker();
		void decode(){}
	};

	/**
	* Class to preprocess an image
	*/
	private class Preprocessor
	{
	private:
		std::map<std::string, Mat> _processedImages;
		Parameters _params;
		void CLAHE(Mat src, Mat& dst);
		void TVD(Mat src, Mat& dst);
		void unsharp(Mat src, Mat& dst);
	public:
		/**
		* Default constructor
		*/
		Preprocessor(){}

		/**
		* Method returns all images from each step of preprocessing
		* @return map of name of a preprocessing step and image after this step
		*/
		std::map<std::string, Mat> getImages(){ return _processedImages; }

		/**
		* Methods adds the name of the preprocessing step and image to 
		* container of all images
		* @param name - name of the preprocessing step
		* @param img - image after this step of preprocessing
		*/
		void addImage(string name, Mat img){ _processedImages.insert(imgPair(name, img)); }

		/**
		* Method sets the preprocessing parameters
		* @param p - structure of algorithm parameters
		*/
		void setParameters(Parameters p);

		/**
		* Method initializes preprocessing algorithm
		* @param img - image to preprocess
		*/
		void preprocess(Mat img);
	};
	
	/**
	* Class to detect marker on image
	*/
	private class Detector
	{
	private: Preprocessor _prep;
			 Mat _idealMarker;
			 vector<vector<cv::Point>> _contours;
			 ContourDetector* _c;
	public:
		/**
		* Default constructor
		*/
		Detector()
		{
			_c = new ContourDetector();
		}

		/**
		* Methods sets an image where marker should be detected
		* @param img - source image
		*/
		void setIdealMarker(Mat img){ _idealMarker = img; }

		/**
		* Method releases a source image
		*/
		void releaseIdealMarker(){ _idealMarker.release(); }

		/**
		* Method performs a preprocessing of image
		* @param p - structure of algorithm parameters
		*/
		void makePreprocessing(Parameters p);

		/**
		* Method returns the results of detecting (images, detecting parameters, 
		* detected codes and bounding rectangles coordinates)
		* @param processedImgs - map of preprocessing step names and images after them
		* @param codes - container of detected codes and bounding rectangles
		* @return success/failure flag
		*/
		bool getResult(std::map<std::string, Mat>& processedImgs,
			System::Collections::Generic::Dictionary<unsigned, Drawing::Rectangle>% codes);

		/**
		* Destructor
		*/
		~Detector()
		{
			releaseIdealMarker();
			delete _c;
		}
	};

	/**
	* Class to operate with processed images
	*/
	public ref class ProcessedImages
	{
		System::Collections::Generic::SortedDictionary<System::String^, array<Byte>^> _images;
	public:
		/**
		* Default constructor
		*/
		ProcessedImages(){ _images.Clear(); }
		
		/**
		* Methods adds the name of the preprocessing step and image to
		* container of all images
		* @param name - name of the preprocessing step
		* @param img - image after this step of preprocessing
		*/
		void addImage(std::string name, cv::Mat img);

		/**
		* Method returns all images from each step of preprocessing
		* @param processed - container of preprocessing step names and images after each step
		*/
		void getAllImages(System::Collections::Generic::SortedDictionary<System::String^, array<Byte>^>% processed);

		/**
		* Method returns image by the preprocessing step name
		* @param imageName - name of preprocessing step
		* @param image - image to return
		* @return success/failure flag
		*/
		bool getImageByName(System::String^ imageName, array<Byte>^% image);
	};

	/**
	* Class to operate with detecting results
	*/
	public ref class DetParamsAndResults
	{
		Parameters _params;
		unsigned _code;
		Drawing::Rectangle _boundRect;
	public:
		/**
		* Mathod stores the current preprocessing parameters
		*/
		void setParameters(Parameters param)
		{
			_params = param;
		}

		/**
		* Getter/setter for code detected from marker
		*/
		property unsigned detectedCode
		{
			unsigned get(){ return _code; }
			void set(unsigned cd){ _code = cd; }
		}

		/**
		* Getter/setter for bounding rectangle of detected marker
		*/
		property Drawing::Rectangle boundingRect
		{
			Drawing::Rectangle get(){ return _boundRect; }
			void set(Drawing::Rectangle br){ _boundRect = br; }
		}

		/**
		* Mathod returns the stored preprocessing parameters
		*/
		void getParameters(Parameters& p)
		{
			p = _params;
		}
	};

	/**
	* Main class of detecting and decoding algorithm
	*/
	public ref class DetectorAndDecoder
	{
	private:
		Decoder* _dcdr = new Decoder();
		Detector* _dtctr = new Detector();
	public:
		/**
		* Method reads an image and performs attempt of detecting and decoding a marker
		* @param imPath - full path to image
		* @param processedImgs - container of detecting parameters and results 
		*                        and preprocessed images from each step of preprocessing
		* @param rows - number of rows in image
		* @param cols - number of columns in image
		*/
		bool Detect(System::String^ imPath,
			System::Collections::Generic::Dictionary<DetParamsAndResults^, ProcessedImages^>% processedImgs, 
			int% rows, int% cols);

		/**
		* Destructor
		*/
		~DetectorAndDecoder()
		{
			delete _dcdr;
			delete _dtctr;
		}
	};
}