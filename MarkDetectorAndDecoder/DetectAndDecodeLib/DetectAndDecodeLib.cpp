// Main DLL-file.

#include "stdafx.h"
#include <msclr/marshal_cppstd.h>
#include "DetectAndDecodeLib.h"
#include "golay24.h"
#include <string>
#include <algorithm>
#include <functional>
#include <numeric>
#include <vector>
#include <bitset>
#include <iostream>
#include "opencv2/photo/photo.hpp" 

using namespace DetectAndDecodeLib;
using namespace std;

typedef System::Collections::Generic::KeyValuePair<System::String^, array<System::Byte>^> imRetPair;
typedef System::Collections::Generic::KeyValuePair<DetParamsAndResults^, ProcessedImages^> imsNParams;

/**
* Method converts char array (OpenCV Mat) to managed byte array
* @param input - data of OpenCV Mat to convert
* @param len - length of input array
* @return managed array of bytes
*/
array<Byte>^ MakeManagedArray(unsigned char* input, int len)
{
	array<Byte>^ result = gcnew array<Byte>(len);
	for (int i = 0; i < len; i++)
	{
		result[i] = input[i];
	}
	return result;
}

/**
* Class to add floating number to char with scale
*/
class AddFloatToCharScaled{
public:
	/**
	* Default constructor
	*/
	AddFloatToCharScaled(double scale) :_scale(scale){}

	/**
	* Method adds floating number to char with scale
	* @param a - floating number to add
	* @param b - char number which should be scaled and added
	* @return double sum
	*/
	inline double operator()(double a, uchar b){
		return a + _scale*((double)b);
	}
private:
	double _scale;
};

/**
* Method performs a Total Variation Denoising algorithm
* @param observations - vector of input images
* @param result - denoised image
* @param lambda - regularization parameter (smallest parameter - blurrest picture)
* @param niters - number of iterations
*/
void denoise_TVL1(const std::vector<Mat>& observations, Mat& result, double lambda, int niters){

	CV_Assert(observations.size() > 0 && niters > 0 && lambda > 0);

	const double L2 = 8.0, tau = 0.02, sigma = 1. / (L2*tau), theta = 1.0;
	double clambda = (double)lambda;
	double s = 0;
	const int workdepth = CV_64F;

	int i, x, y, rows = observations[0].rows, cols = observations[0].cols, count;
	for (i = 1; i < (int)observations.size(); i++){
		CV_Assert(observations[i].rows == rows && observations[i].cols == cols);
	}

	Mat X, P = Mat::zeros(rows, cols, CV_MAKETYPE(workdepth, 2));
	observations[0].convertTo(X, workdepth, 1. / 255);
	std::vector< Mat_<double> > Rs(observations.size());
	for (count = 0; count < (int)Rs.size(); count++){
		Rs[count] = Mat::zeros(rows, cols, workdepth);
	}

	for (i = 0; i < niters; i++)
	{
		double currsigma = i == 0 ? 1 + sigma : sigma;

		// P_ = P + sigma*nabla(X)
		// P(x,y) = P_(x,y)/max(||P(x,y)||,1)
		for (y = 0; y < rows; y++)
		{
			const double* x_curr = X.ptr<double>(y);
			const double* x_next = X.ptr<double>(std::min(y + 1, rows - 1));
			Point2d* p_curr = P.ptr<Point2d>(y);
			double dx, dy, m;
			for (x = 0; x < cols - 1; x++)
			{
				dx = (x_curr[x + 1] - x_curr[x])*currsigma + p_curr[x].x;
				dy = (x_next[x] - x_curr[x])*currsigma + p_curr[x].y;
				m = 1.0 / std::max(std::sqrt(dx*dx + dy*dy), 1.0);
				p_curr[x].x = dx*m;
				p_curr[x].y = dy*m;
			}
			dy = (x_next[x] - x_curr[x])*currsigma + p_curr[x].y;
			m = 1.0 / std::max(std::abs(dy), 1.0);
			p_curr[x].x = 0.0;
			p_curr[x].y = dy*m;
		}


		//Rs = clip(Rs + sigma*(X-imgs), -clambda, clambda)
		for (count = 0; count < (int)Rs.size(); count++){
			std::transform<MatIterator_<double>, MatConstIterator_<uchar>, MatIterator_<double>, AddFloatToCharScaled>(
				Rs[count].begin(), Rs[count].end(), observations[count].begin<uchar>(),
				Rs[count].begin(), AddFloatToCharScaled(-sigma / 255.0));
			Rs[count] += sigma*X;
			min(Rs[count], clambda, Rs[count]);
			max(Rs[count], -clambda, Rs[count]);
		}

		for (y = 0; y < rows; y++)
		{
			double* x_curr = X.ptr<double>(y);
			const Point2d* p_curr = P.ptr<Point2d>(y);
			const Point2d* p_prev = P.ptr<Point2d>(std::max(y - 1, 0));

			// X1 = X + tau*(-nablaT(P))
			x = 0;
			s = 0.0;
			for (count = 0; count < (int)Rs.size(); count++){
				s = s + Rs[count](y, x);
			}
			double x_new = x_curr[x] + tau*(p_curr[x].y - p_prev[x].y) - tau*s;
			// X = X2 + theta*(X2 - X)
			x_curr[x] = x_new + theta*(x_new - x_curr[x]);


			for (x = 1; x < cols; x++)
			{
				s = 0.0;
				for (count = 0; count < (int)Rs.size(); count++){
					s += Rs[count](y, x);
				}
				// X1 = X + tau*(-nablaT(P))
				x_new = x_curr[x] + tau*(p_curr[x].x - p_curr[x - 1].x + p_curr[x].y - p_prev[x].y) - tau*s;
				// X = X2 + theta*(X2 - X)
				x_curr[x] = x_new + theta*(x_new - x_curr[x]);
			}
		}
	}

	result.create(X.rows, X.cols, CV_8U);
	X.convertTo(result, CV_8U, 255);
}

bool DetectorAndDecoder::Detect(System::String^ imPath,
	System::Collections::Generic::Dictionary<DetParamsAndResults^,
	ProcessedImages^>% processedImgs, int% rows, int% cols)
{
	string path = msclr::interop::marshal_as< std::string >(imPath);
	// Filling parameters structure (these are the optimal)
	Parameters p;
	p.tvdLambda = 0.5;
	p.tvdNiters = 60;
	p.umAmount = 10;
	p.umSigma = 15;
	p.umThreshold = 5;
	p.retinaKern = 3;
	p.medianKernelSize = 5;
	p.thresholdBlockSize = 391;
	p.thresholdC = -4;

	System::Collections::Generic::Dictionary<unsigned, Drawing::Rectangle> code;
	// Reading of source image
	Mat _srcImg = imread(path);
	// Setting it as image to preprocess
	_dtctr->setIdealMarker(_srcImg);
	DetParamsAndResults^ params = gcnew DetParamsAndResults();
	ProcessedImages^ imgs = gcnew ProcessedImages();
	// Preprocessing image
	_dtctr->makePreprocessing(p);
	map<string, Mat> HprocessedImgs;
	// Trying to decode information
	if (!_dtctr->getResult(HprocessedImgs, code)) return false;
	int index = 0;
	// Filling of processed images container
	for (auto it = HprocessedImgs.begin(); it != HprocessedImgs.end(); ++it)
	{
		int length = it->second.rows*it->second.cols;
		imgs->addImage(it->first, it->second);
		rows = it->second.rows;
		cols = it->second.cols;
		index++;
	}
	// Filling of container to return
	for each (Collections::Generic::KeyValuePair<unsigned, Drawing::Rectangle> pr in code)
	{
		params = gcnew DetParamsAndResults();
		// Add current parameters
		params->setParameters(p);
		// Add detected codes
		params->detectedCode = pr.Key;
		// Add bounding rectangles
		params->boundingRect = pr.Value;
		processedImgs.Add(params, imgs);
		delete params;
	}
	delete imgs;
	_dtctr->releaseIdealMarker();
	_srcImg.release();
	return true;
}

void Detector::makePreprocessing(Parameters params)
{
	// Applying parameters
	_prep.setParameters(params);
	// Perform preprocessing
	_prep.preprocess(_idealMarker);
}

/**
* Method returns average intensity from specific area
* @param img - source image
* @param center - center of area on image where intensity should be calculated
* @param areaSize - size of area on image where intensity should be calculated
* @return average intensity from specific area
*/
int getAverageIntensity(Mat& img, Point2f center, int areaSize)
{
	string s = to_string(center.x + center.y);
	int mean_level;

	if (areaSize > 2)
	{
		try
		{
			Rect region(Point2f(center.x - areaSize / 2.0f, center.y - areaSize / 2.0f),
				Size(areaSize, areaSize));

			if ((region.x < (img.cols - 1)) && ((region.x + region.width)<img.cols - 1)
				&& (region.y < (img.rows - 1)) && ((region.y + region.height) <(img.rows - 1))
				&& (region.x>0) && ((region.x + region.width)>0)
				&& (region.y>0) && ((region.y + region.height)>0))
			{
				Mat sub_scene(img(region));
				mean_level = (int)mean(sub_scene)[0];
				sub_scene.release();
			}
			else mean_level = 0;
		}
		catch (cv::Exception&)
		{
			mean_level = 0;
		}
	}
	else
	{
		mean_level = (int)img.at<uchar>((int)(center.y + 0.5), (int)(center.x + 0.5));
	}

	return mean_level;
}

/**
* Method returns code decoded from marker
* @param image - source image
* @param keyPoints - array of detected keypoints
* @return decoded code
*/
unsigned int getOneCodeFromKeyPoints(Mat& image, vector<Point> keyPoints)
{
	vector<unsigned> bits;
	unsigned code = 0;
	int averSize = 3;
	// Calculate the black intensity level
	int zeroLevel = getAverageIntensity(image, keyPoints[SMALL_1_CIRCLE_CENTER], averSize) +
		getAverageIntensity(image, keyPoints[SMALL_2_CIRCLE_CENTER], averSize) +
		getAverageIntensity(image, keyPoints[SMALL_3_CIRCLE_CENTER], averSize) +
		getAverageIntensity(image, keyPoints[SMALL_4_CIRCLE_CENTER], averSize) +
		getAverageIntensity(image, keyPoints[SMALL_5_CIRCLE_CENTER], averSize);

	zeroLevel /= 5;

	int smallCircleX = (keyPoints[LEAF_1_RIGHT].x + keyPoints[LEAF_6_LEFT].x) / 2;
	int smallCircleY = (keyPoints[LEAF_1_RIGHT].y + keyPoints[LEAF_6_LEFT].y) / 2;
	// Calculate the white intensity level
	int oneLevel = getAverageIntensity(image, Point(smallCircleX, smallCircleY), averSize);
	// Debug printing
	/*cout << "1 right " << keyPoints[LEAF_1_RIGHT].x << "," << keyPoints[LEAF_1_RIGHT].y<<endl;
	cout << "6 left " << keyPoints[LEAF_6_LEFT].x << "," << keyPoints[LEAF_6_LEFT].y << endl;
	cout << "X: " << smallCircleX << " Y: " << smallCircleY << endl;
	cout << "zero: " << zeroLevel << endl;
	cout << "one: " << oneLevel << endl;*/

	////////////////////////////
	// Get bits from first leaf
	vector<Point2f> allBitsPos;

	vector<Point2f> bitsRegion1_3;
	bitsRegion1_3.push_back(keyPoints[CENTER_1]);
	bitsRegion1_3.push_back(keyPoints[LEAF_1_LEFT]);
	bitsRegion1_3.push_back(keyPoints[LEAF_1_RIGHT]);
	bitsRegion1_3.push_back(keyPoints[CENTER_0_WITHOUT_CIRCLE]);
	vector<Point2f> bitsRegionOr1_3;
	bitsRegionOr1_3.push_back(OR_CENTER_1);
	bitsRegionOr1_3.push_back(OR_LEAF_1_LEFT);
	bitsRegionOr1_3.push_back(OR_LEAF_1_RIGHT);
	bitsRegionOr1_3.push_back(OR_CENTER_0_WITHOUT_CIRCLE);

	vector<Point2f> bitsPosOr;
	bitsPosOr.push_back(BIT_1_POS);
	bitsPosOr.push_back(BIT_2_POS);
	bitsPosOr.push_back(BIT_3_POS);

	Mat bTranform = findHomography(bitsRegionOr1_3, bitsRegion1_3);
	vector<Point2f> bits_current_positions;
	perspectiveTransform(bitsPosOr, bits_current_positions, bTranform);

	for (unsigned int l = 0; l < bits_current_positions.size(); l++)
		allBitsPos.push_back(bits_current_positions[l]);
	
	////////////////////////////
	// Get bits from second leaf
	vector<Point2f> bitsRegion4_6;
	bitsRegion4_6.push_back(keyPoints[CENTER_2]);
	bitsRegion4_6.push_back(keyPoints[LEAF_2_LEFT]);
	bitsRegion4_6.push_back(keyPoints[LEAF_2_RIGHT]);
	bitsRegion4_6.push_back(keyPoints[CENTER_1]);
	vector<Point2f> bitsRegionOr4_6;
	bitsRegionOr4_6.push_back(OR_CENTER_2);
	bitsRegionOr4_6.push_back(OR_LEAF_2_LEFT);
	bitsRegionOr4_6.push_back(OR_LEAF_2_RIGHT);
	bitsRegionOr4_6.push_back(OR_CENTER_1);

	bitsPosOr.clear();
	bitsPosOr.push_back(BIT_4_POS);
	bitsPosOr.push_back(BIT_5_POS);
	bitsPosOr.push_back(BIT_6_POS);

	bTranform = findHomography(bitsRegionOr4_6, bitsRegion4_6);
	bits_current_positions.clear();
	perspectiveTransform(bitsPosOr, bits_current_positions, bTranform);

	for (unsigned int l = 0; l < bits_current_positions.size(); l++)
		allBitsPos.push_back(bits_current_positions[l]);

	///////////////////////////
	// Get bits from third leaf
	vector<Point2f> bitsRegion7_9;
	bitsRegion7_9.push_back(keyPoints[CENTER_3]);
	bitsRegion7_9.push_back(keyPoints[LEAF_3_LEFT]);
	bitsRegion7_9.push_back(keyPoints[LEAF_3_RIGHT]);
	bitsRegion7_9.push_back(keyPoints[CENTER_2]);
	vector<Point2f> bitsRegionOr7_9;
	bitsRegionOr7_9.push_back(OR_CENTER_3);
	bitsRegionOr7_9.push_back(OR_LEAF_3_LEFT);
	bitsRegionOr7_9.push_back(OR_LEAF_3_RIGHT);
	bitsRegionOr7_9.push_back(OR_CENTER_2);

	bitsPosOr.clear();
	bitsPosOr.push_back(BIT_7_POS);
	bitsPosOr.push_back(BIT_8_POS);
	bitsPosOr.push_back(BIT_9_POS);

	bTranform = findHomography(bitsRegionOr7_9, bitsRegion7_9);
	bits_current_positions.clear();
	perspectiveTransform(bitsPosOr, bits_current_positions, bTranform);

	for (unsigned int l = 0; l < bits_current_positions.size(); l++)
		allBitsPos.push_back(bits_current_positions[l]);

	////////////////////////////
	// Get bits from fourth leaf
	vector<Point2f> bitsRegion10_12;
	bitsRegion10_12.push_back(keyPoints[CENTER_4]);
	bitsRegion10_12.push_back(keyPoints[LEAF_4_LEFT]);
	bitsRegion10_12.push_back(keyPoints[LEAF_4_RIGHT]);
	bitsRegion10_12.push_back(keyPoints[CENTER_3]);
	vector<Point2f> bitsRegionOr10_12;
	bitsRegionOr10_12.push_back(OR_CENTER_4);
	bitsRegionOr10_12.push_back(OR_LEAF_4_LEFT);
	bitsRegionOr10_12.push_back(OR_LEAF_4_RIGHT);
	bitsRegionOr10_12.push_back(OR_CENTER_3);

	bitsPosOr.clear();
	bitsPosOr.push_back(BIT_10_POS);
	bitsPosOr.push_back(BIT_11_POS);
	bitsPosOr.push_back(BIT_12_POS);

	bTranform = findHomography(bitsRegionOr10_12, bitsRegion10_12);
	bits_current_positions.clear();
	perspectiveTransform(bitsPosOr, bits_current_positions, bTranform);

	for (unsigned int l = 0; l < bits_current_positions.size(); l++)
		allBitsPos.push_back(bits_current_positions[l]);

	///////////////////////////
	// Get bits from fifth leaf
	vector<Point2f> bitsRegion13_15;
	bitsRegion13_15.push_back(keyPoints[CENTER_5]);
	bitsRegion13_15.push_back(keyPoints[LEAF_5_LEFT]);
	bitsRegion13_15.push_back(keyPoints[LEAF_5_RIGHT]);
	bitsRegion13_15.push_back(keyPoints[CENTER_4]);
	vector<Point2f> bitsRegionOr13_15;
	bitsRegionOr13_15.push_back(OR_CENTER_5);
	bitsRegionOr13_15.push_back(OR_LEAF_5_LEFT);
	bitsRegionOr13_15.push_back(OR_LEAF_5_RIGHT);
	bitsRegionOr13_15.push_back(OR_CENTER_4);

	bitsPosOr.clear();
	bitsPosOr.push_back(BIT_13_POS);
	bitsPosOr.push_back(BIT_14_POS);
	bitsPosOr.push_back(BIT_15_POS);

	bTranform = findHomography(bitsRegionOr13_15, bitsRegion13_15);
	bits_current_positions.clear();
	perspectiveTransform(bitsPosOr, bits_current_positions, bTranform);

	for (unsigned int l = 0; l < bits_current_positions.size(); l++)
		allBitsPos.push_back(bits_current_positions[l]);

	///////////////////////////
	// Get bits from first leaf
	vector<Point2f> bitsRegion16_18;
	bitsRegion16_18.push_back(keyPoints[CENTER_0_WITHOUT_CIRCLE]);
	bitsRegion16_18.push_back(keyPoints[LEAF_6_LEFT]);
	bitsRegion16_18.push_back(keyPoints[LEAF_6_RIGHT]);
	bitsRegion16_18.push_back(keyPoints[CENTER_5]);
	vector<Point2f> bitsRegionOr16_18;
	bitsRegionOr16_18.push_back(OR_CENTER_0_WITHOUT_CIRCLE);
	bitsRegionOr16_18.push_back(OR_LEAF_6_LEFT);
	bitsRegionOr16_18.push_back(OR_LEAF_6_RIGHT);
	bitsRegionOr16_18.push_back(OR_CENTER_5);

	bitsPosOr.clear();
	bitsPosOr.push_back(BIT_16_POS);
	bitsPosOr.push_back(BIT_17_POS);
	bitsPosOr.push_back(BIT_18_POS);

	bTranform = findHomography(bitsRegionOr16_18, bitsRegion16_18);
	bits_current_positions.clear();
	perspectiveTransform(bitsPosOr, bits_current_positions, bTranform);

	for (unsigned int l = 0; l < bits_current_positions.size(); l++)
		allBitsPos.push_back(bits_current_positions[l]);

	////////////////////////////
	// Get bits from sixth leaf
	vector<Point2f> bitsRegion19_24;
	bitsRegion19_24.push_back(keyPoints[CENTER_0_WITHOUT_CIRCLE]);
	bitsRegion19_24.push_back(keyPoints[CENTER_1]);
	bitsRegion19_24.push_back(keyPoints[CENTER_2]);
	bitsRegion19_24.push_back(keyPoints[CENTER_3]);
	bitsRegion19_24.push_back(keyPoints[CENTER_4]);
	bitsRegion19_24.push_back(keyPoints[CENTER_5]);
	vector<Point2f> bitsRegionOr19_24;
	bitsRegionOr19_24.push_back(OR_CENTER_0_WITHOUT_CIRCLE);
	bitsRegionOr19_24.push_back(OR_CENTER_1);
	bitsRegionOr19_24.push_back(OR_CENTER_2);
	bitsRegionOr19_24.push_back(OR_CENTER_3);
	bitsRegionOr19_24.push_back(OR_CENTER_4);
	bitsRegionOr19_24.push_back(OR_CENTER_5);

	bitsPosOr.clear();
	bitsPosOr.push_back(OR_CENTER_AND_BIT_19_COORD);
	bitsPosOr.push_back(BIT_20_POS);
	bitsPosOr.push_back(BIT_21_POS);
	bitsPosOr.push_back(BIT_22_POS);
	bitsPosOr.push_back(BIT_23_POS);
	bitsPosOr.push_back(BIT_24_POS);

	bTranform = findHomography(bitsRegionOr19_24, bitsRegion19_24);
	bits_current_positions.clear();
	perspectiveTransform(bitsPosOr, bits_current_positions, bTranform);

	for (unsigned int l = 0; l < bits_current_positions.size(); l++)
		allBitsPos.push_back(bits_current_positions[l]);

	// Bits probing
	for (unsigned int m = 0; m < allBitsPos.size(); m++)
	{
		int curr_level = getAverageIntensity(image, allBitsPos[m], averSize);

		int distance_to_low_level = abs(curr_level - zeroLevel);
		int distance_to_high_level = abs(curr_level - oneLevel);

		unsigned int bit_value = 0;

		if (distance_to_low_level < distance_to_high_level)bit_value = 0;
		else bit_value = 1;

		bits.push_back(bit_value);
	}

	for (unsigned int sh = 0; sh < bits.size(); sh++)
	{
		if (bits[sh] == 1)
		{
			code += (1 << sh);
		}
	}

	// Decoding of Golay code
	code = Golay::decodeGolay(code);
	return code;
}

void Preprocessor::setParameters(Parameters p)
{
	_params = p;
	_processedImages.clear();
}

/**
* Deprecated method of gamma correction
* @param src - source image
* @param dst - image with corrected gamma
* @param fGamma - gamma coefficient
*/
void GammaCorrection(Mat& src, Mat& dst, float fGamma)
{
	unsigned char lut[256];
	for (int i = 0; i < 256; i++)
	{
		lut[i] = saturate_cast<uchar>(pow((float)(i / 255.0), fGamma) * 255.0f);
	}
	dst = src.clone();
	const int channels = dst.channels();
	switch (channels)
	{
	case 1:
	{
			  MatIterator_<uchar> it, end;
			  for (it = dst.begin<uchar>(), end = dst.end<uchar>(); it != end; it++)
				  *it = lut[(*it)];
			  break;
	}
	case 3:
	{
			  MatIterator_<Vec3b> it, end;
			  for (it = dst.begin<Vec3b>(), end = dst.end<Vec3b>(); it != end; it++)
			  {
				  (*it)[0] = lut[((*it)[0])];
				  (*it)[1] = lut[((*it)[1])];
				  (*it)[2] = lut[((*it)[2])];
			  }
			  break;
	}
	}
}

/**
* Method performs the Contrast Limited Adaptive Histogram Equalisation algorithm
* @param src - source image
* @param dst - processed image
*/
void Preprocessor::CLAHE(Mat src, Mat& dst)
{
	cv::Mat lab_image;
	cv::cvtColor(src, lab_image, CV_BGR2Lab);

	// Extract the L channel
	std::vector<cv::Mat> lab_planes(3);
	cv::split(lab_image, lab_planes);  // now we have the L image in lab_planes[0]

	// apply the CLAHE algorithm to the L channel
	cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
	clahe->setClipLimit(_params.retinaKern);
	cv::Mat out;
	clahe->apply(dst, out);

	// Merge the the color planes back into an Lab image
	out.copyTo(dst);
	// Uncomment these strings if image was color
	//cv::merge(lab_planes, lab_image);
	// convert back to RGB
	//cv::cvtColor(lab_image, image_clahe, CV_Lab2BGR);

	//image_clahe.convertTo(image_clahe, CV_8UC3);
}

/**
* Method performs the Total Variation Denoising algorithm
* @param src - source image
* @param dst - processed image
*/
void Preprocessor::TVD(Mat src, Mat& dst)
{
	vector<Mat> nnn = { src.clone() };
	denoise_TVL1(nnn, dst, _params.tvdLambda, _params.tvdNiters);//30
}

/**
* Method performs the Unsharp Mask algorithm
* @param src - source image
* @param dst - processed image
*/
void Preprocessor::unsharp(Mat src, Mat& dst)
{
	Mat blurred;
	double sigma = _params.umSigma, amount = _params.umAmount;
	GaussianBlur(src, blurred, Size(), sigma, sigma);
	Mat lowContrastMask = abs(src - blurred) < _params.umThreshold;
	dst = src*(1 + amount) + blurred*(-amount);
	src.copyTo(dst, lowContrastMask);
	medianBlur(dst, dst, _params.medianKernelSize);
	blurred.release();
	lowContrastMask.release();
}

void Preprocessor::preprocess(Mat img)
{
	//addImage("0. Source", img);
	cvtColor(img, img, CV_RGB2GRAY);
	normalize(img, img, 0, 255, CV_MINMAX);
	img.convertTo(img, CV_8UC1);
	addImage("1. Gray image", img.clone());

	cv::Mat gray_3channels(img.rows, img.cols, CV_8UC3);
	cv::Mat inm[] = { img, img, img };
	int from_to[] = { 0, 0, 1, 1, 2, 2 };
	mixChannels(inm, 3, &gray_3channels, 1, from_to, 3);

	this->CLAHE(gray_3channels, img);
	addImage("2. CLAHE", img.clone());

	TVD(img, img);
	addImage("3. TVD", img.clone());

	unsharp(img, img);
	addImage("4. Unsharpened", img.clone());

	adaptiveThreshold(img, img, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY,
		_params.thresholdBlockSize, _params.thresholdC);
	addImage("5. Thresholded", img.clone());
}

bool Detector::getResult(map<string, Mat>& processedImgs,
	System::Collections::Generic::Dictionary<unsigned, System::Drawing::Rectangle>% code)
{
	processedImgs = _prep.getImages();
	auto p = processedImgs.end();
	p--;
	Mat t = p->second.clone();

	Mat contours = Mat::zeros(t.rows, t.cols, CV_8UC1);
	_c->getImWithContours(t, contours, _contours, 0);
	_prep.addImage("6. All conoturs", contours);
	contours.release();

	Mat fcontours = Mat::zeros(t.rows, t.cols, CV_8UC1);
	_c->getImWithContours(t, fcontours, _contours, 1);
	_prep.addImage("7. Filtered contours", fcontours);
	t.release();

	Mat kp = fcontours.clone();
	fcontours.release();
	vector<vector<Point> > pt;
	_c->getKeyPoints(pt);
	Scalar color = Scalar(250, 250, 250);
	bool isFounded = 0;
	for each (vector<Point> markPt in pt)
	{
		if (markPt.size() < 2) continue;
		for (int i = 0; i < markPt.size(); i++) {
			circle(kp, markPt[i], 8, color);
		}
		isFounded = 1;
	}
	if (!isFounded) return false;
	_prep.addImage("8. Image with keypoints", kp);

	Mat res = processedImgs.begin()->second.clone();
	vector<int> areas;
	for (int i = 0; i < _contours.size(); i++) {
		areas.push_back(contourArea(_contours[i]));
	}

	processedImgs = _prep.getImages();
	auto newp = processedImgs.begin();
	advance(newp, 4);
	unsigned tempCode = 0;
	Mat f = newp->second.clone();
	try {
		for each (vector<Point> currentMarker in pt)
		{
			if (currentMarker.size() > 23)
			{
				// Trying to get code from each keypoints set
				Rect brect = boundingRect(currentMarker);
				tempCode = getOneCodeFromKeyPoints(f, currentMarker);
				code.Add(tempCode, System::Drawing::Rectangle(brect.x, brect.y, brect.width, brect.height));
			}
			else break;
		}
		f.release();
	}
	catch (...) {
		return false;
	}

	return true;
}

void ProcessedImages::addImage(std::string name, cv::Mat img)
{
	System::String^ imName = gcnew System::String(name.c_str());
	this->_images.Add(imName, MakeManagedArray(img.data, img.total()*img.elemSize()));
}

void ProcessedImages::getAllImages(System::Collections::Generic::SortedDictionary<System::String^, array<Byte>^>% processed)
{
	processed.Clear();
	for each (System::Collections::Generic::KeyValuePair<System::String^, array<Byte>^> entry in this->_images)
	{
		processed.Add(entry.Key, entry.Value);
	}
}

bool ProcessedImages::getImageByName(System::String^ imageName, array<Byte>^% image)
{
	array<Byte>^ tmp;
	if (!this->_images.TryGetValue(imageName, tmp)) return false;
	image = tmp;
	return true;
}

void ContourDetector::getImWithContours(Mat src, Mat& dst, vector<vector<Point> >& Contours,
	bool contFiltration)
{
	getAllContours(src);
	dst = Mat::zeros(Size(src.cols, src.rows), src.type());
	Contours = contours;
	if (contFiltration)
	{
		getFilteredContours();
		Contours = fContours;
	}

	// Draw contours
	for (int i = 0; i < Contours.size(); i++)
	{
		Scalar color = Scalar(255, 255, 255);
		drawContours(dst, Contours, i, color, 1, 8, hierarchy, 0, Point());
	}
}

void ContourDetector::getAllContours(Mat src)
{
	// Find contours
	findContours(src, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_NONE, Point(0, 0));
}

/**
* Method finds the nearest keypoint to current one
* @param convexPts - vector of keypoints
* @param pt - current point
* @return index of nearest point to current one
*/
int findNearestPt(vector<Point>& convexPts, Point pt)
{
	int index = 0;
	double min = sqrt((pt.x - convexPts[0].x)*(pt.x - convexPts[0].x) +
		(pt.y - convexPts[0].y)*(pt.y - convexPts[0].y));
	double curDist = 0;
	for (int i = 1; i < convexPts.size(); i++)
	{
		curDist = sqrt((pt.x - convexPts[i].x)*(pt.x - convexPts[i].x) +
			(pt.y - convexPts[i].y)*(pt.y - convexPts[i].y));
		if (curDist < min)
		{
			min = curDist;
			index = i;
		}
	}
	return index;
}

/**
* Method finds the keypoints on cross contour and store it in right order
* @param contour - vector of contours
* @param keyPts - vector of keypoints
* @return success/failure flag
*/
bool findConexivityDefects(vector<Point> contour, vector<Point>& keyPts)
{
	vector<Point> hull(contour.size());
	vector<int> inthull(contour.size());
	vector<Vec4i> defs(contour.size());
	vector<Point> convexPts;
	// Get the keypoints in angles
	approxPolyDP(contour, convexPts, arcLength(Mat(contour), true)*0.01, true);

	// Calculate the keypoint in center of a cross
	int sumX = 0, sumY = 0;
	vector <int> Xs;
	vector <int> Ys;
	for (int i = 0; i < 9; i++)
	{
		Xs.push_back(abs(convexPts[i].x + convexPts[i + 9].x) / 2);
		Ys.push_back(abs(convexPts[i].y + convexPts[i + 9].y) / 2);
	}
	sort(Xs.begin(), Xs.end());
	sort(Ys.begin(), Ys.end());
	// Writing center keypoint
	keyPts.push_back(Point(Xs.at(Xs.size() / 2), Ys.at(Ys.size() / 2)));

	Point closest;
	Point finclosest;
	// Calculating nearest point to angle without a circle to define writing order
	int thrDist = 0;
	for (int i = 0; i < 9; i += 3)
	{
		thrDist += sqrt((convexPts[i].x - convexPts[i + 9].x)*(convexPts[i].x - convexPts[i + 9].x) +
			(convexPts[i].y - convexPts[i + 9].y)*(convexPts[i].y - convexPts[i + 9].y)) / 3;
	}
	thrDist /= 6;

	bool found = 0;
	for (int i = 0; i < 5; i++)
	{
		closest.x = keyPts[CENTER_AND_BIT_19_COORD].x + (keyPts[CENTER_AND_BIT_19_COORD].x - keyPts[i].x);
		closest.y = keyPts[CENTER_AND_BIT_19_COORD].y + (keyPts[CENTER_AND_BIT_19_COORD].y - keyPts[i].y);
		found = 0;
		for (int j = 0; j < 5; j++)
		{
			if ((abs(keyPts[j].x - closest.x) < thrDist) && (abs(keyPts[j].y - closest.y) < thrDist))
			{
				found = 1;
			}
		}
		if (!found)
		{
			finclosest.x = closest.x;
			finclosest.y = closest.y;
		}
	}

	// Writing all keypoints
	int smallCircleX = keyPts[CENTER_AND_BIT_19_COORD].x + (keyPts[CENTER_AND_BIT_19_COORD].x -
		(keyPts[SMALL_1_CIRCLE_CENTER].x + keyPts[SMALL_2_CIRCLE_CENTER].x +
		keyPts[SMALL_3_CIRCLE_CENTER].x + keyPts[SMALL_4_CIRCLE_CENTER].x +
		keyPts[SMALL_5_CIRCLE_CENTER].x) / 5) * 2.5;
	int smallCircleY = keyPts[CENTER_AND_BIT_19_COORD].y + (keyPts[CENTER_AND_BIT_19_COORD].y -
		(keyPts[SMALL_1_CIRCLE_CENTER].y + keyPts[SMALL_2_CIRCLE_CENTER].y +
		keyPts[SMALL_3_CIRCLE_CENTER].y + keyPts[SMALL_4_CIRCLE_CENTER].y +
		keyPts[SMALL_5_CIRCLE_CENTER].y) / 5) * 2.5;
	Point beg = Point(smallCircleX, smallCircleY);
	int nearInd = findNearestPt(convexPts, beg/*finclosest*/);
	Point tmp = convexPts[nearInd];
	for (int i = nearInd; i < convexPts.size(); i++)
	{
		tmp = convexPts[i];
		keyPts.push_back(tmp);
	}
	for (int i = 0; i < nearInd; i++)
	{
		tmp = convexPts[i];
		keyPts.push_back(tmp);
	}

	return true;
}

void ContourDetector::getFilteredContours()
{
	int minCircleArea = 35;
	int minLength = 25;
	int minArea = 20;
	int minDataLength = 7;
	int minDataArea = 5;
	double minRingRatio = 1.8;
	double maxRingRatio = 2.5;
	double ringToArea = 2.5;
	int prev(1), next(0), parent(3), none(-1), firstChild(2);
	RNG rng(12345);

	int index = 0;
	Vec4i curLevel;
	vector<Point> points;
	vector<RotatedRect> minRect(contours.size());

	for (auto curCont = contours.begin(); curCont != contours.end(); ++curCont)
	{
		if ((curCont->size() < minLength) || (curCont->size() == 0))
		{
			index++;
			continue;
		}
		double curContArea = contourArea(*curCont);
		if (curContArea < minCircleArea)
		{
			index++;
			continue;
		}
		Vec4i curLevel = hierarchy[index];

		if ((curLevel[parent] != none) &&
			(curLevel[next] == none) &&
			(curLevel[prev] == none))
		{
			if ((hierarchy[curLevel[parent]][parent] != none) &&
				(curLevel[firstChild] != none) &&
				(hierarchy[curLevel[firstChild]][next] != none))
			{
				int inCircle = index;
				int outerCircle = curLevel[parent];

				int smallCirc = curLevel[firstChild];
				int ind = 0;
				while (smallCirc != none)
				{
					if (ind != 5)
					{
						if ((contours[smallCirc].size() < minLength) ||
							(contours[smallCirc].size() == 0))
						{
							smallCirc = hierarchy[smallCirc][next];
							continue;
						}
						curContArea = contourArea(contours[smallCirc]);
						if (curContArea < minArea)
						{
							smallCirc = hierarchy[smallCirc][next];
							continue;
						}
						fContours.push_back(contours[smallCirc]);
						minRect[smallCirc] = minAreaRect(contours[smallCirc]);
						points.push_back(minRect[smallCirc].center);
					}
					else break;
					smallCirc = hierarchy[smallCirc][next];
					ind++;
				}
				int cross = smallCirc;
				if ((cross == none))
				{
					index++;
					continue;
				}
				else if ((hierarchy[cross][firstChild] == none))
				{
					index++;
					continue;
				}
				int data = hierarchy[cross][firstChild];
				while (data != none)
				{
					if ((contours[data].size() < minDataLength) || (contours[data].size() == 0))
					{
						data = hierarchy[data][next];
						continue;
					}
					curContArea = contourArea(contours[data]);
					if (curContArea < minDataArea)
					{
						data = hierarchy[data][next];
						continue;
					}
					fContours.push_back(contours[data]);
					data = hierarchy[data][next];
				}

				fContours.push_back(contours[cross]);
				fContours.push_back(contours[inCircle]);
				fContours.push_back(contours[outerCircle]);

				findConexivityDefects(contours[cross], points);
				_points.push_back(points);
				points.clear();
			}
		}

		index++;
	}
}

void ContourDetector::getKeyPoints(vector<vector<Point> >& points)
{
	points = _points;
}
