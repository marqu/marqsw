// ConsoleDecoder.cpp: main file of the project.

#include "stdafx.h"

using namespace System;
using namespace System::Collections;
using namespace DetectAndDecodeLib;

int main(array<System::String ^> ^args)
{
	DetectorAndDecoder^ dnd = gcnew DetectorAndDecoder();
	Generic::Dictionary<DetParamsAndResults^, ProcessedImages^> sd;
	int rows, cols;
	if (!dnd->Detect(args[0], sd, rows, cols))
	{
		Console::WriteLine(-1);
		return 0;
	}
	for each (Generic::KeyValuePair<DetParamsAndResults^, ProcessedImages^> entry in sd)
	{
		if ((int)entry.Key->detectedCode != 0)
		{
			Console::Write(entry.Key->detectedCode + " ");
		}
	}
	Console::Write("\n");
	return 0;
}
