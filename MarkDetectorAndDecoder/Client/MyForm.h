#pragma once
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"


namespace Client {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace DetectAndDecodeLib;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			listView1->View = View::Details;
			listView1->Columns->Add("0", "�����������");
			listView1->AutoResizeColumns(ColumnHeaderAutoResizeStyle::HeaderSize);
			listView2->View = View::Details;
			listView2->Columns->Add("0", "�");
			listView2->Columns->Add("1", "���� ���������� �������");
			listView2->Columns->Add("2", "���� ������� ������");
			listView2->Columns->Add("3", "�������� ������");
			listView2->AutoResizeColumns(ColumnHeaderAutoResizeStyle::HeaderSize);
			label1->Text = "���: ";
		}

	protected:
		/// <summary>
		/// Release used resources
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::ListView^  listView1;
	private: System::Windows::Forms::PictureBox^  pictureBox2;
	private: System::Windows::Forms::ListView^  listView2;
	private: System::Windows::Forms::Label^  label1;
	protected:

	private:
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->listView1 = (gcnew System::Windows::Forms::ListView());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->listView2 = (gcnew System::Windows::Forms::ListView());
			this->label1 = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(12, 12);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(259, 35);
			this->button1->TabIndex = 0;
			this->button1->Text = L"������� �����������";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// pictureBox1
			// 
			this->pictureBox1->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox1->Location = System::Drawing::Point(13, 54);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(258, 237);
			this->pictureBox1->TabIndex = 1;
			this->pictureBox1->TabStop = false;
			// 
			// listView1
			// 
			this->listView1->GridLines = true;
			this->listView1->Location = System::Drawing::Point(278, 54);
			this->listView1->Name = L"listView1";
			this->listView1->Size = System::Drawing::Size(211, 237);
			this->listView1->TabIndex = 2;
			this->listView1->UseCompatibleStateImageBehavior = false;
			this->listView1->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::listView1_MouseClick);
			// 
			// pictureBox2
			// 
			this->pictureBox2->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox2->Location = System::Drawing::Point(495, 54);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(260, 237);
			this->pictureBox2->TabIndex = 3;
			this->pictureBox2->TabStop = false;
			// 
			// listView2
			// 
			this->listView2->Location = System::Drawing::Point(13, 298);
			this->listView2->Name = L"listView2";
			this->listView2->Size = System::Drawing::Size(742, 200);
			this->listView2->TabIndex = 4;
			this->listView2->UseCompatibleStateImageBehavior = false;
			this->listView2->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::listView2_MouseClick);
			// 
			// label1
			// 
			this->label1->Location = System::Drawing::Point(495, 33);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(260, 13);
			this->label1->TabIndex = 5;
			this->label1->Text = L"label1";
			this->label1->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(767, 510);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->listView2);
			this->Controls->Add(this->pictureBox2);
			this->Controls->Add(this->listView1);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->button1);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private:
		Generic::Dictionary<DetParamsAndResults^, ProcessedImages^> sd;
		int rows, cols;
		int paramIndex;
		System::Void button1_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void listView1_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
		System::Void listView2_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);

		/**
		* This method converts OpenCV Mat to Bitmap
		* @param srcImg - image to convert
		* @return image in bitmap
		*/
		System::Drawing::Bitmap^ MatToBitmap(cv::Mat srcImg);
	};
}
