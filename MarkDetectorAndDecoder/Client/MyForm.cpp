#include "MyForm.h"

using namespace System;
using namespace System::Windows::Forms;
using namespace Client;

[STAThread]
int main(array<System::String ^> ^args)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	MyForm form;
	Application::Run(%form);
	return 0;
}

/**
* This method converts byte array to OpenCV Mat
* @param byteArray - byte array to convert
* @param output - Mat image
* @param rows - number of rows in output image
* @param cols - number of cols in output image
* @param stride - stride of output image
*/
void byteArray2Mat(array<System::Byte>^ byteArray, cv::Mat &output, int rows, int cols, int stride)
{
	pin_ptr<System::Byte> p = &byteArray[0];
	unsigned char* pby = p;
	char* pch = reinterpret_cast<char*>(pby);

	if (stride == 3)
		output = cv::Mat(rows, cols, CV_8UC3, (void*)pch);
	else
		output = cv::Mat(rows, cols, CV_8UC1, (void*)pch);
}

/**
* This method converts System::String to std::string
* @param s - string to convert
* @param os - output string
*/
void MarshalString(String ^ s, std::string& os)
{
	using namespace Runtime::InteropServices;
	const char* chars =
		(const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
	os = chars;
	Marshal::FreeHGlobal(IntPtr((void*)chars));
}

System::Drawing::Bitmap^ MyForm::MatToBitmap(cv::Mat srcImg)
{
	int stride = srcImg.size().width * srcImg.channels();	// calc the srtide
	int hDataCount = srcImg.size().height;

	System::Drawing::Bitmap^ retImg;

	System::IntPtr ptr(srcImg.data);

	// create a pointer with Stride
	if (stride % 4 != 0){	// is not stride a multiple of 4?
		// make it a multiple of 4 by fiiling an offset to the end of each row

		// to hold processed data
		uchar *dataPro = new uchar[((srcImg.size().width * srcImg.channels() + 3) & -4) * hDataCount];

		uchar *data = srcImg.ptr();

		// current position on the data array
		int curPosition = 0;
		// current offset
		int curOffset = 0;

		int offsetCounter = 0;

		// iterate through all the bytes on the structure
		for (int r = 0; r < hDataCount; r++){
			// fill the data
			for (int c = 0; c < stride; c++){
				curPosition = (r * stride) + c;

				dataPro[curPosition + curOffset] = data[curPosition];
			}

			// reset offset counter
			offsetCounter = stride;

			// fill the offset
			do{
				curOffset += 1;
				dataPro[curPosition + curOffset] = 0;

				offsetCounter += 1;
			} while (offsetCounter % 4 != 0);
		}

		ptr = (System::IntPtr)dataPro;	// set the data pointer to new/modified data array

		// calc the stride to nearest number which is a multiply of 4
		stride = (srcImg.size().width * srcImg.channels() + 3) & -4;

		retImg = gcnew System::Drawing::Bitmap(srcImg.size().width, srcImg.size().height,
			stride,
			System::Drawing::Imaging::PixelFormat::Format24bppRgb,
			ptr);
	}
	else{
		// no need to add a padding or recalculate the stride
		retImg = gcnew System::Drawing::Bitmap(srcImg.size().width, srcImg.size().height,
			stride,
			System::Drawing::Imaging::PixelFormat::Format24bppRgb,
			ptr);
	}

	array<Byte>^ imageData;
	System::Drawing::Bitmap^ output;

	// Create the byte array.
	{
		System::IO::MemoryStream^ ms = gcnew System::IO::MemoryStream();
		retImg->Save(ms, System::Drawing::Imaging::ImageFormat::Png);
		imageData = ms->ToArray();
		delete ms;
	}

	// Convert back to bitmap
	{
		System::IO::MemoryStream^ ms = gcnew System::IO::MemoryStream(imageData);
		output = (System::Drawing::Bitmap^)System::Drawing::Bitmap::FromStream(ms);
	}

	return output;
}

System::Void MyForm::button1_Click(System::Object^  sender, System::EventArgs^  e) {
	OpenFileDialog^ ofd = gcnew OpenFileDialog();
	ofd->Filter = "�����������(*.jpg, *.bmp, *.png)|*.jpg;*.bmp;*.png";
	ofd->Title = "�������� �����������";
	// Open the "Open File" dialog
	if (ofd->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		String^ filePath = ofd->FileName;
		listView1->Clear();
		listView1->View = View::Details;
		listView1->FullRowSelect = true;
		listView1->Columns->Add("0", "�����������");
		listView1->AutoResizeColumns(ColumnHeaderAutoResizeStyle::HeaderSize);

		listView2->Clear();

		Graphics^ g = pictureBox1->CreateGraphics();
		g->Clear(Control::BackColor);

		try
		{
			this->Text = filePath;
			DetectorAndDecoder^ dnd = gcnew DetectorAndDecoder();
			sd.Clear();
			// Initialize decoding process
			dnd->Detect(filePath, sd, rows, cols);
			int g = 0;
			int m(0), b(0), c(0);
			// Filling the parameters column names
			listView2->View = View::Details;
			listView2->Columns->Add("0", "�");
			listView2->Columns->Add("1", "CLAHE kernel");
			listView2->Columns->Add("2", "TVD lambda");
			listView2->Columns->Add("3", "TVD number of iterations");
			listView2->Columns->Add("4", "UM sigma");
			listView2->Columns->Add("5", "UM threshold");
			listView2->Columns->Add("6", "UM amount");
			listView2->Columns->Add("7", "Median kernel");
			listView2->Columns->Add("8", "Threshold block size");
			listView2->Columns->Add("9", "Threshold constant");
			listView2->FullRowSelect = true;
			listView2->AutoResizeColumns(ColumnHeaderAutoResizeStyle::HeaderSize);
			int index = 0;
			cv::Mat tmpMat;
			array<Byte>^ tmp;
			Parameters params;
			for each (Generic::KeyValuePair<DetParamsAndResults^, ProcessedImages^> entry in sd)
			{
				index++;
				// Filling the parameters column values
				entry.Key->getParameters(params);
				ListViewItem^ it = gcnew System::Windows::Forms::ListViewItem(index.ToString());
				it->SubItems->Add(params.retinaKern.ToString());
				it->SubItems->Add(params.tvdLambda.ToString());
				it->SubItems->Add(params.tvdNiters.ToString());
				it->SubItems->Add(params.umSigma.ToString());
				it->SubItems->Add(params.umThreshold.ToString());
				it->SubItems->Add(params.umAmount.ToString());
				it->SubItems->Add(params.medianKernelSize.ToString());
				it->SubItems->Add(params.thresholdBlockSize.ToString());
				it->SubItems->Add(params.thresholdC.ToString());
				listView2->Items->Add(it);
			}
			delete dnd;
		}
		catch (System::IO::FileNotFoundException^ ex)
		{
			MessageBox::Show(ex->FileName);
		}
	}
}

System::Void MyForm::listView1_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	ListViewHitTestInfo^ info = listView1->HitTest(e->Location);
	ListViewItem^ item = info->Item;

	int index = -1;
	for each (Generic::KeyValuePair<DetParamsAndResults^, ProcessedImages^> entry in sd)
	{
		index++;
		if (index == paramIndex)
		{
			Generic::SortedDictionary<System::String^, array<Byte>^> imgs;
			entry.Value->getAllImages(imgs);
			cv::Mat tmpMat;
			array<Byte>^ tmp;
			if (imgs.TryGetValue(item->Text, tmp))
			{
				byteArray2Mat(tmp, tmpMat, rows, cols, 1);
				cv::Mat gray_3channels(tmpMat.rows, tmpMat.cols, CV_8UC3);
				cv::Mat in[] = { tmpMat, tmpMat, tmpMat };
				int from_to[] = { 0, 0, 1, 1, 2, 2 };
				mixChannels(in, 3, &gray_3channels, 1, from_to, 3);
				
				Bitmap^ b = MatToBitmap(gray_3channels);
				pictureBox1->SizeMode = PictureBoxSizeMode::Zoom;
				pictureBox1->Image = b;
			}
		}
	}
}

System::Void MyForm::listView2_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	ListViewHitTestInfo^ info = listView2->HitTest(e->Location);
	ListViewItem^ item1 = info->Item;

	int index = -1;
	for each (Generic::KeyValuePair<DetParamsAndResults^, ProcessedImages^> entry in sd)
	{
		index++;
		if (index == item1->Index)
		{
			label1->Text = "���: 0x" + Convert::ToString((int)entry.Key->detectedCode, 16);
			Generic::SortedDictionary<System::String^, array<Byte>^> imgs;
			paramIndex = index;
			listView1->Clear();
			listView1->View = View::Details;

			listView1->FullRowSelect = true;
			listView1->Columns->Add("0", "�����������");
			listView1->AutoResizeColumns(ColumnHeaderAutoResizeStyle::HeaderSize);


			entry.Value->getAllImages(imgs);
			for each (Generic::KeyValuePair<System::String^, array<Byte>^> img in imgs)
			{
				ListViewItem^ itmp = gcnew System::Windows::Forms::ListViewItem(img.Key);
				listView1->Items->Add(itmp);
			}

			cv::Mat tmpMat;
			array<Byte>^ tmp;
			if (imgs.TryGetValue("1. Gray image", tmp))
			{
				byteArray2Mat(tmp, tmpMat, rows, cols, 2);

				cv::Mat gray_3channels(tmpMat.rows, tmpMat.cols, CV_8UC3);
				cv::Mat in[] = { tmpMat, tmpMat, tmpMat };
				int from_to[] = { 0, 0, 1, 1, 2, 2 };
				mixChannels(in, 3, &gray_3channels, 1, from_to, 3);

				Bitmap^ b = MatToBitmap(gray_3channels);
				pictureBox1->SizeMode = PictureBoxSizeMode::Zoom;
				pictureBox1->Image = b;

				listView1->Items[1]->Focused = true;
				listView1->Items[1]->Selected = true;
			}

			if ((int)entry.Key->detectedCode != 0)
			{
				byteArray2Mat(tmp, tmpMat, rows, cols, 2);

				cv::Mat gray_3channels(tmpMat.rows, tmpMat.cols, CV_8UC3);
				cv::Mat in[] = { tmpMat, tmpMat, tmpMat };
				int from_to[] = { 0, 0, 1, 1, 2, 2 };
				mixChannels(in, 3, &gray_3channels, 1, from_to, 3);

				Bitmap^ b = MatToBitmap(gray_3channels);
				Graphics^ g = Graphics::FromImage(b);
				Drawing::Pen^ pen = gcnew Drawing::Pen(Color::Lime);
				pen->Width = rows*0.01;
				g->DrawRectangle(pen, entry.Key->boundingRect);
				pictureBox2->SizeMode = PictureBoxSizeMode::Zoom;
				pictureBox2->Image = b;
			}
		}
	}
}
