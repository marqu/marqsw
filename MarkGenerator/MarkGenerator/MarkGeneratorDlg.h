
// MarkGeneratorDlg.h : ���� ���������
//

#pragma once
#include "CMarker.h"

// Number of rows and columns in marker
#define MARKER_SIDE 4096

// Dialog window CMarkGeneratorDlg
class CMarkGeneratorDlg : public CDialogEx
{
// Creation
public:
	CMarkGeneratorDlg(CWnd* pParent = NULL);	// Default constructor

// Data of dialog window
	enum { IDD = IDD_MARKGENERATOR_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Support of DDX/DDV


// Realization
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CWnd* picW;
	CDC* pic;
	CRect picR;
	CBitmap bmp, *tmp;
	CDC *mDC;
	int inCircRad;
	int inR;
	int outR;
	int inStarR;
	int outStarR;
	int peaks;
	int dataRadius;
	int* marker;
	CMarker m;
	CString data;
	void draw();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedSave();
};
