#include "stdafx.h"

struct markerParameters
{
	int outerRadius;
	int insideRadius;
	int insideCircleRadius;
	int starPeaksNum;
	int starOuterRadius;
	int starInsideRadius;
	int countPx;
	int dataR;
	int data;
};

/**
* Marker generator class
* @author Egor Serov
*/
class CMarker
{
private:
	markerParameters _params;
	double PI = 3.141596;
	int* _marker;
	int* getStar();
	int* getCircles();
	void getDataCircles(int count,int*starPic);
	int encodeData(int data);
public:
	/**
	* Default constructor
	*/
	CMarker();

	/**
	* Method sets marker parameters described in structure
	* @param parameters - structure that contains marker
	*                     parameters
	*/
	void setParams(markerParameters parameters);

	/**
	* Method returns generated marker
	* @return array of intensivities (0...255)
	*/
	int* getMarker();
};