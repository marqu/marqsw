
// MarkGeneratorDlg.cpp : Realization file
//


#include "stdafx.h"
#include "MarkGenerator.h"
#include "MarkGeneratorDlg.h"
#include "afxdialogex.h"
#include <vector>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// Dialog window CMarkGeneratorDlg



CMarkGeneratorDlg::CMarkGeneratorDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMarkGeneratorDlg::IDD, pParent)
	, inCircRad(150)
	, inR(1600)
	, outR(1850)
	, inStarR(520)
	, outStarR(1500)
	, peaks(6)
	, dataRadius(100)
	, data(_T("0xfabcde"))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMarkGeneratorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, inCircRad);
	DDX_Text(pDX, IDC_EDIT2, inR);
	DDX_Text(pDX, IDC_EDIT3, outR);
	DDX_Text(pDX, IDC_EDIT4, inStarR);
	DDX_Text(pDX, IDC_EDIT5, outStarR);
	DDX_Text(pDX, IDC_EDIT6, peaks);
	DDX_Text(pDX, IDC_EDIT7, dataRadius);
	DDX_Text(pDX, IDC_EDIT8, data);
}

BEGIN_MESSAGE_MAP(CMarkGeneratorDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CMarkGeneratorDlg::OnBnClickedOk)
//	ON_BN_CLICKED(IDCANCEL2, &CMarkGeneratorDlg::OnBnClickedCancel2)
ON_BN_CLICKED(IDC_SAVE, &CMarkGeneratorDlg::OnBnClickedSave)
END_MESSAGE_MAP()


// Message handlers CMarkGeneratorDlg

BOOL CMarkGeneratorDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Sets the icon of dialog window. Environments makes it automatically,
	// if the main window is not dialog.
	SetIcon(m_hIcon, TRUE);			// Large icon
	SetIcon(m_hIcon, FALSE);		// Small icon

	picW = GetDlgItem(IDC_MARKER);
	pic = picW->GetDC();
	picW->GetClientRect(&picR);
	mDC = new CDC;
	mDC->CreateCompatibleDC(pic);
	bmp.CreateCompatibleBitmap(pic, picR.Width(), picR.Height());
	tmp = mDC->SelectObject(&bmp);
	GetDlgItem(IDC_SAVE)->EnableWindow(0);
	return TRUE;
}

void CMarkGeneratorDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Context of drawing

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Align the icon in center of user's rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{		
		pic->BitBlt(1, 1, picR.Width() - 2, picR.Height() - 2, mDC, 1, 1, SRCCOPY);
		CDialogEx::OnPaint();
	}
}

HCURSOR CMarkGeneratorDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

double* bilinIn(int* oldPic, int oldW, int oldH, int newW, int newH)
{
	double* temp = new double[newW*newH];
	double A, B, C, D, val;
	int x, y, index;
	double x_ratio = ((double)(oldW - 1)) / newW;
	double y_ratio = ((double)(oldH - 1)) / newH;
	double x_diff, y_diff, ya, yb;
	int offset = 0;
	for (int i = 0; i < newH; i++)
	{
		for (int j = 0; j < newW; j++)
		{
			x = (int)(x_ratio * j);
			y = (int)(y_ratio * i);
			x_diff = (x_ratio * j) - x;
			y_diff = (y_ratio * i) - y;
			index = y*oldW + x;

			A = oldPic[index];
			B = oldPic[index + 1];
			C = oldPic[index + oldW];
			D = oldPic[index + oldW + 1];

			val = A*(1 - x_diff)*(1 - y_diff) + B*(x_diff)*(1 - y_diff) + 
				  C*(y_diff)*(1 - x_diff) + D*(x_diff*y_diff);

			temp[offset++] = val;
		}
	}
	return temp;
}

void CMarkGeneratorDlg::draw()
{
	mDC->FillSolidRect(picR, RGB(255,255,255));

	double* circle = 
		bilinIn(marker, MARKER_SIDE, MARKER_SIDE, picR.Width(), picR.Height());
	for (int i = 0; i < picR.Height(); i++)
	{
		for (int j = 0; j < picR.Width(); j++)
		{
			mDC->SetPixel(i, j, RGB(circle[i *picR.Width() + j], 
				circle[i * picR.Width() + j], circle[i * picR.Width() + j]));
		}
	}
	mDC->TextOutW(5,5, data);
	OnPaint();
}

BOOL saveMarkerAsBMP(int* data, int width,wstring buf)
{
	CImage img;
	img.Create(width, width, 32);
	for (int i = 0; i<width; i++)
	{
		for (int j = 0; j<width; j++)
		{
			img.SetPixelRGB(i, j, data[i*width + j], data[i*width + j], data[i*width + j]);
		}
	}
	buf+=L"/marker.bmp";
	img.Save(buf.c_str());
	return true;
}

void CMarkGeneratorDlg::OnBnClickedOk()
{
	UpdateData(1);
	markerParameters parameters;
	parameters.countPx = MARKER_SIDE * MARKER_SIDE;
	parameters.insideCircleRadius = inCircRad;
	parameters.insideRadius = inR;
	parameters.outerRadius = outR;
	parameters.starInsideRadius = inStarR;
	parameters.starOuterRadius = outStarR;
	parameters.starPeaksNum = peaks*1.5;
	parameters.dataR = dataRadius;
	parameters.data = wcstol(data, NULL, 0);
	m.setParams(parameters);
	GetDlgItem(IDOK)->EnableWindow(0);
	GetDlgItem(IDC_SAVE)->EnableWindow(0);
	marker = m.getMarker();
	GetDlgItem(IDOK)->EnableWindow(1);
	GetDlgItem(IDC_SAVE)->EnableWindow(1);
	draw();
	UpdateData(0);
}

void CMarkGeneratorDlg::OnBnClickedSave()
{
	BROWSEINFO inf;
	inf.hwndOwner = AfxGetApp()->m_pMainWnd->m_hWnd;
	inf.pidlRoot = NULL;
	inf.lpszTitle = _T("�������� ����� ��� ���������� �����������...");
	inf.lpfn = NULL;
	inf.lParam = NULL;
	inf.pszDisplayName = NULL;
	inf.ulFlags = BIF_RETURNONLYFSDIRS;
	inf.iImage = NULL;
	InitCommonControls();
	ITEMIDLIST *itls;
	char buf[MAX_PATH];
	if ((itls = SHBrowseForFolder(&inf)) != NULL)
		SHGetPathFromIDList(itls, (LPWSTR)buf);
	GetDlgItem(IDOK)->EnableWindow(0);
	GetDlgItem(IDC_SAVE)->EnableWindow(0);
	saveMarkerAsBMP(marker, MARKER_SIDE, (LPWSTR)buf);
	GetDlgItem(IDOK)->EnableWindow(1);
	GetDlgItem(IDC_SAVE)->EnableWindow(1);
}
