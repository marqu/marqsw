#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <opencv2\opencv.hpp>
#include <windows.h>
#include <string>
#include <time.h>
#include <random>
#include <ctime>

using namespace std;
using namespace cv;

struct params{
	double first;
	double second;
};

/**
* Enumeration of available modificators
*/
enum Modificator{scale, gaussianBlur, gaussianNoise, 
	saltAndPepper, illumIrreg, rotation, axisCompression,
	perspectiveTrans, multipleMarks};

mt19937 _gen;

// This function was taken from http://jepsonsblog.blogspot.ru/2012/11/rotation-in-3d-using-opencvs.html
void rotateImage(const Mat &input, Mat &output, double alpha, 
	double beta, double gamma, double dx, double dy, double dz, double f)
{
	alpha = (alpha - 90.)*CV_PI / 180.;
	beta = (beta - 90.)*CV_PI / 180.;
	gamma = (gamma - 90.)*CV_PI / 180.;

	// get width and height for ease of use in matrices
	double w = (double)input.cols;
	double h = (double)input.rows;

	// Projection 2D -> 3D matrix
	Mat A1 = (Mat_<double>(4, 3) <<
		1, 0, -w / 2,
		0, 1, -h / 2,
		0, 0, 0,
		0, 0, 1);

	// Rotation matrices around the X, Y, and Z axis
	Mat RX = (Mat_<double>(4, 4) <<
		1, 0, 0, 0,
		0, cos(alpha), -sin(alpha), 0,
		0, sin(alpha), cos(alpha), 0,
		0, 0, 0, 1);

	Mat RY = (Mat_<double>(4, 4) <<
		cos(beta), 0, -sin(beta), 0,
		0, 1, 0, 0,
		sin(beta), 0, cos(beta), 0,
		0, 0, 0, 1);

	Mat RZ = (Mat_<double>(4, 4) <<
		cos(gamma), -sin(gamma), 0, 0,
		sin(gamma), cos(gamma), 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1);

	// Composed rotation matrix with (RX, RY, RZ)
	Mat R = RX * RY * RZ;

	// Translation matrix
	Mat T = (Mat_<double>(4, 4) <<
		1, 0, 0, dx,
		0, 1, 0, dy,
		0, 0, 1, dz,
		0, 0, 0, 1);

	// 3D -> 2D matrix
	Mat A2 = (Mat_<double>(3, 4) <<
		f, 0, w / 2, 0,
		0, f, h / 2, 0,
		0, 0, 1, 0);

	// Final transformation matrix
	Mat trans = A2 * (T * (R * A1));

	// Apply matrix transformation
	warpPerspective(input, output, trans, input.size(), INTER_LANCZOS4);
}

/**
* Method of scale modificator
* @param refImg - source image
* @param param - parameters of distortion
* @return modified image
*/
Mat Scale(Mat refImg, params param)
{
	Mat tmp;
	double newR = refImg.rows * param.first;
	double newC = refImg.cols * param.first;
	Size newSize = { (long)newR, (long)newC };
	resize(refImg, tmp, newSize, 0, 0, CV_INTER_CUBIC);
	return tmp;
}

/**
* Method of axis compression modificator
* @param refImg - source image
* @param param - parameters of distortion
* @return modified image
*/
Mat AxisCompression(Mat refImg, params param)
{
	Mat tmp;
	double newC = refImg.cols / param.first * 100;
	Size newSize = { (long)refImg.rows, (long)newC };
	resize(refImg, tmp, newSize, 0, 0, CV_INTER_CUBIC);
	return tmp;
}

/**
* Method of Gaussian blur modificator
* @param refImg - source image
* @param param - parameters of distortion
* @return modified image
*/
Mat GaussianBlur(Mat refImg, params param)
{
	Mat tmp = refImg.clone();
	param.first *= (tmp.cols + tmp.rows) / 2;
	if ((int)param.first % 2 == NULL)
	{
		GaussianBlur(tmp, tmp, Size(param.first + 1, param.first + 1), 0);
	}
	else
	{
		GaussianBlur(tmp, tmp, Size(param.first, param.first), 0);
	}
	return tmp;
}

/**
* Method of Gaussian noise modificator
* @param refImg - source image
* @param param - parameters of distortion
* @return modified image
*/
Mat GaussianNoise(Mat refImg, params param)
{
	Mat tmp = refImg.clone();
	cvtColor(tmp, tmp, 7);
	tmp.convertTo(tmp, CV_32FC1);
	Mat noise = tmp.clone();
	noise.setTo(0);
	param.first *= (tmp.cols + tmp.rows) / 2;
	randn(noise, 0, param.first);
	tmp += noise;
	normalize(tmp, tmp, 0, 255, CV_MINMAX);
	noise.release();
	return tmp;
}

/**
* Method of Salt and Pepper noise modificator
* @param refImg - source image
* @param param - parameters of distortion
* @return modified image
*/
Mat SaltAndPepper(Mat refImg, params param)
{
	Mat tmp = refImg.clone();
	param.first *= param.first * tmp.rows * tmp.cols;
	for (UINT k = 0; k < param.first; k++)
	{
		int z = rand() % tmp.cols;
		int j = rand() % tmp.rows;
		if (tmp.channels() == 1)
		{
			tmp.at<uchar>(j, z) = 255;
		}
		else if (tmp.channels() == 3)
		{
			tmp.at<Vec3b>(j, z)[0] = 255;
			tmp.at<Vec3b>(j, z)[1] = 255;
			tmp.at<Vec3b>(j, z)[2] = 255;
		}
	}
	for (UINT u = 0; u < param.first; u++)
	{
		int bi = rand() % tmp.cols;
		int bj = rand() % tmp.rows;
		if (tmp.channels() == 1)
		{
			tmp.at<uchar>(bi, bj) = 0;
		}
		else if (tmp.channels() == 3)
		{
			tmp.at<Vec3b>(bj, bi)[0] = 0;
			tmp.at<Vec3b>(bj, bi)[1] = 0;
			tmp.at<Vec3b>(bj, bi)[2] = 0;
		}
	}
	return tmp;
}

/**
* Method of illumination irregularity modificator
* @param refImg - source image
* @param param - parameters of distortion
* @return modified image
*/
Mat IlluminationIrregularity(Mat refImg, params param)
{
	Mat tmp = refImg.clone();
	cvtColor(tmp, tmp, 7);
	tmp.convertTo(tmp, CV_8UC1);
	Mat grad = tmp.clone();
	grad.setTo(0);
	bool min = 0;
	if (param.first < 0)
	{
		min = 1;
		Point2f pt(grad.cols / 2., grad.rows / 2.);
		Mat r = getRotationMatrix2D(pt, 180, 1.0);
		warpAffine(grad, grad, r, Size(grad.cols, grad.rows));
		r.release();
	}
	for (int y = grad.rows - 1; y >= 0; --y)
	{
		unsigned char *const scanLine = grad.ptr<unsigned char>(y);

		for (int x = grad.cols - 1; x >= 0; --x)
		{
			scanLine[x] = scanLine[x] + ((fabs(param.first) / 200)*y);
		}
	}
	if (min)
	{
		Point2f pt(grad.cols / 2., grad.rows / 2.);
		Mat r = getRotationMatrix2D(pt, -180, 1.0);
		warpAffine(grad, grad, r, Size(grad.cols, grad.rows));
		r.release();
	}
	tmp += grad;
	grad.release();
	return tmp;
}

/**
* Method of rotation modificator
* @param refImg - source image
* @param param - parameters of distortion
* @return modified image
*/
Mat Rotation(Mat refImg, params param)
{
	Mat src, frame, frameRotated;
	src = refImg.clone();
	int diagonal = (int)sqrt(src.cols*src.cols + src.rows*src.rows);
	int newWidth = diagonal;
	int newHeight = diagonal;
	int offsetX = (newWidth - src.cols) / 2;
	int offsetY = (newHeight - src.rows) / 2;
	Mat targetMat(newWidth, newHeight, src.type());
	Point2f src_center(targetMat.cols / 2.0F, targetMat.rows / 2.0F);
	src.copyTo(frame);
	frame.copyTo(targetMat.rowRange(offsetY, offsetY + frame.rows).colRange(offsetX, offsetX + frame.cols));
	Mat rot_mat = getRotationMatrix2D(src_center, param.first, 1.0);
	warpAffine(targetMat, frameRotated, rot_mat, targetMat.size(), 1, BORDER_TRANSPARENT);
	Rect bound_Rect(frame.cols, frame.rows, 0, 0);
	int x1 = offsetX;
	int x2 = offsetX + frame.cols;
	int x3 = offsetX;
	int x4 = offsetX + frame.cols;
	int y1 = offsetY;
	int y2 = offsetY;
	int y3 = offsetY + frame.rows;
	int y4 = offsetY + frame.rows;
	Mat co_Ordinate = (Mat_<double>(3, 4) << x1, x2, x3, x4,
		y1, y2, y3, y4,
		1, 1, 1, 1);
	Mat RotCo_Ordinate = rot_mat * co_Ordinate;
	for (int i = 0; i < 4; i++){
		if (RotCo_Ordinate.at<double>(0, i) < bound_Rect.x)
			bound_Rect.x = (int)RotCo_Ordinate.at<double>(0, i); //access smallest 
		if (RotCo_Ordinate.at<double>(1, i)<bound_Rect.y)
			bound_Rect.y = RotCo_Ordinate.at<double>(1, i); //access smallest y
	}
	for (int i = 0; i<4; i++){
		if (RotCo_Ordinate.at<double>(0, i)>bound_Rect.width)
			bound_Rect.width = (int)RotCo_Ordinate.at<double>(0, i); //access largest x
		if (RotCo_Ordinate.at<double>(1, i)>bound_Rect.height)
			bound_Rect.height = RotCo_Ordinate.at<double>(1, i); //access largest y
	}
	bound_Rect.width = bound_Rect.width - bound_Rect.x;
	bound_Rect.height = bound_Rect.height - bound_Rect.y;
	Mat cropedResult;
	Mat ROI = frameRotated(bound_Rect);
	ROI.copyTo(cropedResult);
	ROI.release();
	src.release();
	frame.release();
	frameRotated.release();
	co_Ordinate.release();
	RotCo_Ordinate.release();
	rot_mat.release();
	return cropedResult;
}

/**
* Method of perspective transform modificator
* @param refImg - source image
* @param param - parameters of distortion
* @return modified image
*/
Mat PerspectiveTransform(Mat refImg, params param)
{
	Mat tmp;
	resize(refImg, tmp, Size(refImg.rows*1.25, refImg.cols*1.25), 0, 0, CV_INTER_CUBIC);
	tmp.setTo(0);
	refImg.copyTo(tmp(Rect(refImg.rows*0.125, refImg.cols*0.125, refImg.rows, refImg.cols)));
	rotateImage(tmp, tmp, 90, 90 - param.first, 90, 0, 0, tmp.cols, tmp.cols);
	return tmp;
}

/**
* Method of multiple markers modificator
* @param refImg - source image
* @param param - parameters of distortion
* @return modified image
*/
Mat MultipleMarkers(Mat refImg, params param)
{
	Mat tmp = refImg.clone();
	uniform_real_distribution<> angleDist(0, 360);
	double angle = angleDist(_gen);
	Mat rot_mat = getRotationMatrix2D(Point(tmp.cols / 2, tmp.rows / 2), angle, 1.0);
	warpAffine(tmp, tmp, rot_mat, tmp.size(), 1, BORDER_TRANSPARENT);
	uniform_real_distribution<> dist(param.first, param.second);
	double scale = dist(_gen);
	double newR = tmp.rows*scale;
	double newC = tmp.cols*scale;
	Size newSize = { (long)newR, (long)newC };
	resize(tmp, tmp, newSize, 0, 0, CV_INTER_CUBIC);

	return tmp;
}

/**
* Method performs one specified distortion
* @param refImg - source image
* @param param - parameters of distortion
* @param mod - specified distortion
* @return modified image
*/
Mat process(Mat refImg, params param, Modificator mod)
{
	Mat result;
	switch (mod)
	{
	case scale:result = Scale(refImg, param); break;
	case gaussianBlur:result = GaussianBlur(refImg, param); break;
	case gaussianNoise:result = GaussianNoise(refImg, param); break;
	case saltAndPepper:result = SaltAndPepper(refImg, param); break;
	case illumIrreg:result = IlluminationIrregularity(refImg, param); break;
	case rotation:result = Rotation(refImg, param); break;
	case perspectiveTrans:result = PerspectiveTransform(refImg, param); break;
	case multipleMarks:result = MultipleMarkers(refImg, param); break;
	case axisCompression:result = AxisCompression(refImg, param); break;
	}
	return result;
}

/**
* Method to count time
* @return string with current time
*/
const std::string currentDateTime() {
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);
	strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

	return buf;
}

/**
* Method to perform multiple marker modification
* @param folderToRead - full path to folder with denerated markers
* @param background - full path to background image
* @param output - full path to directory where results should be saved
* @param maxImages - maximum number of markers on one image
*/
void multipleMarkersMain(string folderToRead, string background, string output, int maxImages)
{
	WIN32_FIND_DATA FindFileData;
	HANDLE hf;
	char buf[32];
	if (folderToRead == "") return;
	int count = 1;
	double step = 2;
	string fulWritePath;
	params scale;
	string command = "";
	string begDate = currentDateTime();
	Mat bkgnd = imread(background);
	int rowNum = 0, colNum = 0;
	int index = 0;
	uniform_int_distribution<> pointDist;
	wstring filter = wstring(folderToRead.begin(), folderToRead.end()) + L"*.bmp";

	int maxImageCount = maxImages;
	int sideCells = sqrt(maxImageCount) + 1;
	vector<Point> cells;
	double scaleR = bkgnd.rows / sideCells;
	double scaleC = bkgnd.cols / sideCells;

	Point cellPoint;
	int pointNum;
	string codeName;
	int iiii = 0;

	for (int num = 1; num < maxImageCount + 0.5; num++)
	{
		for (int aver = 0; aver < 250; aver++)
		{
			for (int i = 0; i < sideCells; i++)
			{
				for (int j = 0; j < sideCells; j++)
				{
					cells.push_back(Point(i*scaleC, j*scaleR));
				}
			}

			string writePath = output + to_string(num);
			command = "mkdir " + writePath;
			if (aver == 0)system(command.c_str());

			hf = FindFirstFile(filter.c_str(), &FindFileData);
			if (hf != INVALID_HANDLE_VALUE)
			{
				fulWritePath = writePath + "\\" + to_string(aver + 1);
				do
				{
					size_t len = 
						wcstombs(buf, FindFileData.cFileName, wcslen(FindFileData.cFileName));

					if (len > 0u)
						buf[len] = '\0';
					Mat tmp = imread(folderToRead + buf);
					scale.first = 0.16;
					scale.second = 0.5;

					codeName = buf;
					cout << codeName << endl;
					codeName.replace(codeName.find(".bmp", iiii), 4, "");
					fulWritePath += "_" + codeName;
					
					tmp = process(tmp, scale, multipleMarks);

					pointNum = rand() % (cells.size());
					cellPoint = cells.at(pointNum);
					auto it = cells.begin();
					advance(it, pointNum);
					cells.erase(it);
					
					rowNum = cellPoint.y + (rand() % (int)scaleR) / ((maxImageCount+2-1)/2);
					if (rowNum > bkgnd.rows - tmp.rows)rowNum = bkgnd.rows - tmp.rows - 5;
					if (rowNum < 0)rowNum = 0;
					
					colNum = cellPoint.x + (rand() % (int)scaleC) / ((maxImageCount + 2 - 1) / 2);
					if (colNum > bkgnd.cols - tmp.cols)colNum = bkgnd.cols - tmp.cols - 5;
					if (colNum < 0)colNum = 0;
					tmp.copyTo(bkgnd(cv::Rect(colNum, rowNum, tmp.cols, tmp.rows)));

					if (count%num == 0)
					{
						imwrite(fulWritePath+".bmp", bkgnd);
						bkgnd = imread(background/*, CV_8UC1*/);

						count = 1;
						index = 0;
						tmp.setTo(0);
						tmp.release();
						break;
					}
					count++;
					tmp.setTo(0);
					tmp.release();
				} while (FindNextFile(hf, &FindFileData) != 0);
				FindClose(hf);
				cells.clear();
			}
		}
	}
		cout << begDate << endl;
		cout << currentDateTime() << endl;
		cout << "\a";
}

/**
* Method to perform single marker modifications
* @param folderToRead - full path to folder with denerated markers
* @param output - full path to directory where results should be saved
* @param mod - specified distortion
* @param start - beginning value of distortion parameter
* @param end - end value of distortion parameter
* @param step - step of changing the value of distortion parameter
*/
void ordinaryMain(string folderToRead, string output, Modificator mod, 
	double start, double end, double step)
{
	WIN32_FIND_DATA FindFileData;
	HANDLE hf;
	char buf[32];
	if (folderToRead == "") return;
	int count = 1;
	string fulWritePath;
	params scale;
	string command = "";
	string begDate = currentDateTime();
	int rowNum = 0, colNum = 0;
	int index = 0;

	int iiii = 0;
	wstring filter = wstring(folderToRead.begin(), folderToRead.end()) + L"*.bmp";
	
	hf = FindFirstFile(filter.c_str(), &FindFileData);
	if (hf != INVALID_HANDLE_VALUE)
	{
		do
		{
			size_t len = wcstombs(buf, FindFileData.cFileName, wcslen(FindFileData.cFileName));

			if (len > 0u)
				buf[len] = '\0';
			Mat tmp = imread(folderToRead + buf);

			for (double i = start; i <= end; i += step)
			{
				scale.first = i;
				scale.second = i;

				cout << count << ".\t" << buf << "\tparam " << scale.first;

				string writePath = output + to_string(scale.first);
				command = "mkdir " + writePath;
				if (count == 1)system(command.c_str());
				fulWritePath = writePath + "\\" + buf;
				scale.first = i;
				imwrite(fulWritePath, process(tmp, scale, mod));
				cout << " saved" << endl;
			}
			count++;
			tmp.setTo(0);
			tmp.release();
		} while (FindNextFile(hf, &FindFileData) != 0);
		FindClose(hf);
	}
	cout << begDate << endl;
	cout << currentDateTime() << endl;
	cout << "\a";
}

void main(int argc, char* argv[])
{
	string folderToRead = "", background = "", output = "";
	bool multiple = 0;
	int modificator = 0, numOfImages = 0;
	double start = 0, end = 0, step = 0;
	for (int i = 1; i < argc; i++) 
	{ 
		if (argv[i] == string("-g"))
		{
			folderToRead = argv[i + 1];
		}
		else if (argv[i] == string("-m"))
		{
			multiple = 1;
			background = argv[i + 1];
		}
		else if (argv[i] == string("-o"))
		{
			output = argv[i + 1];
		}
		else if (argv[i] == string("-mod"))
		{
			modificator = atoi(argv[i + 1]);
		}
		else if (argv[i] == string("-n"))
		{
			numOfImages = atoi(argv[i + 1]);
		}
		else if (argv[i] == string("-start"))
		{
			start = atof(argv[i + 1]);
		}
		else if (argv[i] == string("-end"))
		{
			end = atof(argv[i + 1]);
		}
		else if (argv[i] == string("-step"))
		{
			step = atof(argv[i + 1]);
		}
	}
	if (multiple)
		multipleMarkersMain(folderToRead, background, output, numOfImages);
	else
		ordinaryMain(folderToRead, output, (Modificator)modificator, start, end, step);
	cout << (Modificator)modificator << endl;
}