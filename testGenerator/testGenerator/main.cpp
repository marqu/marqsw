#include "CMarker.h"
#include "opencv2\opencv.hpp"
#include <string>
#include <iostream>

using namespace std;
using namespace cv;

void main(int argc, char* argv[])
{
	markerParameters parameters;
	vector<int>marker;
	int sideSize = 1024;
	Mat tmp = Mat::zeros(Size(sideSize, sideSize), CV_8UC1);
	parameters.countPx = sideSize * sideSize;
	parameters.insideCircleRadius = 43;
	parameters.insideRadius = 400;
	parameters.outerRadius = 462;
	parameters.starInsideRadius = 130;
	parameters.starOuterRadius = 375;
	parameters.starPeaksNum = 6*1.5;
	parameters.dataR = 25;
	string base = argv[1] + string("marker");
	string numStr;
	string code;
	for (int i = 1; i <= 1000; i++)
	{
		CMarker m;
		parameters.data = i;
		cout << "Marker " << i;
		m.setParams(parameters);
		marker = m.getMarker(code);
		cout << " created,";
		for (int r = 0; r < tmp.rows; r++)
		{
			for (int c = 0; c < tmp.cols; c++)
			{
				tmp.at<uchar>(r, c) = marker[c*tmp.rows + r];
			}
		}
		numStr = "_"+to_string(i)+"_"+code+".bmp";
		imwrite(base + numStr, tmp);
		cout << " saved" << endl;
	}
}