#include<string>
#include <vector>

struct markerParameters
{
	int outerRadius;
	int insideRadius;
	int insideCircleRadius;
	int starPeaksNum;
	int starOuterRadius;
	int starInsideRadius;
	int countPx;
	int dataR;
	int data;
};

struct POINT
{
	int x, y;
};

/**
* Marker generator class
* @author Egor Serov
*/
class CMarker
{
private:
	markerParameters _params;
	std::string _code;
	double PI = 3.141596;
	std::vector<int> _marker;
	int* getStar();
	int* getCircles();
	void getDataCircles(int count,int*starPic);
	int encodeData(int data);
public:
	/**
	* Default constructor
	*/
	CMarker();

	/**
	* Method sets marker parameters described in structure
	* @param parameters - structure that contains marker
	*                     parameters
	*/
	void setParams(markerParameters parameters);

	/**
	* Method returns generated marker
	* @return array of intensivities (0...255)
	*/
	std::vector<int> getMarker(std::string& code);
};