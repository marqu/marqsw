#include "CMarker.h"
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <iomanip>
#include <sstream>
#include "golay24.h"

using namespace std;

CMarker::CMarker()
{
	// Filling default parameters
	_params.countPx = 0;
	_params.insideCircleRadius = 0;
	_params.insideRadius = 0;
	_params.outerRadius = 0;
	_params.starInsideRadius = 0;
	_params.starOuterRadius = 0;
	_params.starPeaksNum = 0;
	_params.data = 0;
}

void CMarker::setParams(markerParameters parameters)
{
	// Filling parameters by values from outside
	_params.countPx = parameters.countPx;
	_params.insideCircleRadius = parameters.insideCircleRadius;
	_params.insideRadius = parameters.insideRadius;
	_params.outerRadius = parameters.outerRadius;
	_params.starInsideRadius = parameters.starInsideRadius;
	_params.starOuterRadius = parameters.starOuterRadius;
	_params.starPeaksNum = parameters.starPeaksNum;
	_params.dataR = parameters.dataR;
	_params.data = parameters.data;
}

/**
* Method to check is point's coordinates belongs some polygon
* @param p - array of polygon's points
* @param Number - number of polygon's angles
* @param x - x coordinate of point to check
* @param y - y coordinate of point to check
* @return 0 - if point doesn't belong to polygon, otherwise - 1
*/
int isPointInsidePolygon(POINT* p, int Number, int x, int y)
{
	int c = 0;
	for (int i = 0, j = Number - 1; i < Number; j = i++)
	{
		if ((((p[i].y <= y) && (y < p[j].y)) || ((p[j].y <= y) && (y<p[i].y))) &&
			(x >(p[j].x - p[i].x) * (y - p[i].y) / (p[j].y - p[i].y) + p[i].x))
			c = !c;
	}
	return c;
}

/**
* Method encodes data with Golay code
* @param data - data to encode
* @return encoded data
*/
int CMarker::encodeData(int data)
{
	return Golay::encodeGolay(data);
}

/**
* Method fills data circles with specified color (0 - black, 1 - white)
* @param countCirc - number of circles
* @param starPic - array of intensivities with colored data circles
*/
void  CMarker::getDataCircles(int countCirc, int*starPic)
{
	double side = sqrt((double)_params.countPx);
	bool* data = new bool[countCirc];
	int x = sqrt((double)_params.countPx) / 2, y = x;
	double a = 1.5* 360. / (2 * _params.starPeaksNum);
	POINT tmp;
	vector<POINT>circles;
	double count = (double)countCirc / (_params.starPeaksNum / 1.5);
	double step = (_params.starOuterRadius - _params.starInsideRadius) / (count - 1);
	int b = 1;
	_params.data = encodeData(_params.data);
	for (int i = sizeof(_params.data) * 8 - 1; i >= 0; i--)
	{
		unsigned z = 1 << i;
		data[i] = _params.data&z;
	}
	// Calculating the coordinates of each circle
	for (int i = 0; i < (_params.starPeaksNum * 2) + 1; i++)
	{
		if (b == 2)
		{
			for (int j = 0; j < count - 1; j++)
			{
				tmp.x = (_params.starOuterRadius - j*step - 2 * _params.dataR)*
					sin(a*PI / 180.) + side / 2.;
				tmp.y = (_params.starOuterRadius - j*step-2*_params.dataR)*
					cos(a*PI / 180.) + side / 2.;
				circles.push_back(tmp);
			}
			b = 0;
		}
		else b++;
		a = a + 180. / (_params.starPeaksNum);
	}
	tmp.x = x;
	tmp.y = y;
	circles.push_back(tmp);
	a = 0;
	int left = countCirc-1 - (count - 1)*(_params.starPeaksNum / 1.5);
	double tmpX(0), tmpY(0);
	step = (_params.starInsideRadius+_params.dataR) / 2;
	for (int i = 0; i <left; i++)
	{
		
			tmp.x = x - step * sin(a * PI / 180.);
			tmp.y = y + step * cos(a * PI / 180.);
			circles.push_back(tmp);
			a = a + 180 / left*2;
	}

	// Coloring circles according their bits (0 - black, 1 - white)
	// The background of circles is black, so coloring only white circles
	for (int k = 0; k < circles.size(); k++)
	{
		for (int i = circles[k].x - _params.dataR; i < circles[k].x + _params.dataR; i++)
		{
			for (int j = circles[k].y - _params.dataR; j < circles[k].y + _params.dataR; j++)
			{
				int index = (i)* side + (j);
				if (((i - circles[k].x)*(i - circles[k].x) + (j - circles[k].y)*
					(j - circles[k].y) < _params.dataR * _params.dataR) && data[k])
					starPic[index] = 255;
			}
		}
	}
}

/**
* Method creates black special figure on image
* @return array of intensities with black special figure
*         with data circles
*/
int* CMarker::getStar()
{
	int a = 0;
	int x = sqrt((double)_params.countPx) / 2, y = x;
	int* star = new int[_params.countPx];
	for (int i = 0; i < _params.countPx; i++)star[i] = 255;
	//Array to store coordinates of figure's angles
	POINT* peaks = new POINT[_params.starPeaksNum * 2 + 1]; 
	// Calculating the angles coordinates
	int b = 0;
	for (int i = 0; i < _params.starPeaksNum * 2 + 1; i++)
	{
		if (b == 2)
		{
			peaks[i].x = x - _params.starInsideRadius * sin(a * PI / 180.);
			peaks[i].y = y + _params.starInsideRadius * cos(a * PI / 180.);
			b = 0;
		}
		else
		{
			peaks[i].x = x - _params.starOuterRadius * sin(a*PI / 180.);
			peaks[i].y = y + _params.starOuterRadius * cos(a*PI / 180.);
			b++;
		}
		a = a + 180 / _params.starPeaksNum;
	}
	// End figure on first point
	peaks[_params.starPeaksNum * 2 + 1].x = peaks[1].x;
	peaks[_params.starPeaksNum * 2 + 1].y = peaks[1].y;

	// Coloring figure by black
	double side = sqrt((double)_params.countPx);
	for (int i = 0; i < side; i++)
	{
		for (int j = 0; j < side; j++)
		{
			int index = (i)* side + (j);
			if (isPointInsidePolygon(peaks, _params.starPeaksNum * 2 + 1, i, j) == 1)
				star[index] = 0;
		}
	}
	// Draw data circles
	getDataCircles(24, star);
	return star;
}

/**
* Method draws outer and small orientation circles
* @return array of intensities with circles
*/
int* CMarker::getCircles()
{
	int* circ = new int[_params.countPx];
	double side = sqrt((double)_params.countPx);
	int shift = 0;

	int a = 0;
	int x = sqrt((double)_params.countPx) / 2, y = x;
	POINT tmp;

	// Drawing the big outer ring
	for (int i = -side / 2; i < side / 2; i++)
	{
		for (int j = -side / 2; j < side / 2; j++)
		{
			int index = (i + side / 2) * side + (j + side / 2);
			if ((i*i + j*j<_params.outerRadius * _params.outerRadius) &&
				(i*i + j*j>_params.insideRadius*_params.insideRadius)) circ[index] = 0;
			else circ[index] = 255;
		}
	}

	// Have to calculate figure's angles again to find a place of orientation circles
	POINT* peaks = new POINT[_params.starPeaksNum * 2 - 1];
	int b = 0;
	for (int i = 0; i < _params.starPeaksNum * 2 - 2; i++)
	{
		if (b == 2)
		{
			peaks[i].x = x - _params.starInsideRadius * sin(a * PI / 180.);
			peaks[i].y = y + _params.starInsideRadius * cos(a * PI / 180.);
			b = 0;
		}
		else
		{
			peaks[i].x = x - _params.starOuterRadius * sin(a*PI / 180.);
			peaks[i].y = y + _params.starOuterRadius * cos(a*PI / 180.);
			b++;
		}
		a = a + 180 / _params.starPeaksNum;
	}
	b = 1;
	// Calculating orientation circles coordinates
	vector<POINT>circles;
	double xA(0), xB(0), xC(0), yA(0), yB(0), yC(0), first(0), sec(0), third(0);
	for (int i = 0; i < _params.starPeaksNum * 2 - 2; i++)
	{
		if (b % 3 == 0)
		{
			xA = peaks[i].x;
			xB = peaks[i - 1].x;
			xC = peaks[i + 1].x;
			yA = peaks[i].y;
			yB = peaks[i - 1].y;
			yC = peaks[i + 1].y;
			third = sqrt((xB - xA)*(xB - xA) + (yB - yA)*(yB - yA));
			first = sqrt((xC - xB)*(xC - xB) + (yC - yB)*(yC - yB));
			sec = sqrt((xA - xC)*(xA - xC) + (yA - yC)*(yA - yC));
			tmp.x = (first*xA + sec*xB + third*xC) / (first + sec + third);
			tmp.y = (first*yA + sec*yB + third*yC) / (first + sec + third);
			circles.push_back(tmp);
			b = 0;
		}
		b++;
		a = a + 180 / _params.starPeaksNum;
	}
	// Coloring circles by black
	for (int k = 0; k < circles.size(); k++)
	{
		for (int i = circles[k].x - _params.insideCircleRadius; i < circles[k].x + _params.insideCircleRadius; i++)
		{
			for (int j = circles[k].y - _params.insideCircleRadius; j < circles[k].y + _params.insideCircleRadius; j++)
			{
				int index = (i)* side + (j);
				if (((i - circles[k].x)*(i - circles[k].x) + (j - circles[k].y)*
					(j - circles[k].y) < _params.insideCircleRadius * _params.insideCircleRadius))
					circ[index] = 0;
			}
		}
	}
	circles.clear();
	delete[]peaks;
	return circ;
}


vector<int> CMarker::getMarker(string& code)
{
	_marker.clear();
	_marker.resize(_params.countPx);
	int* resStar = getStar();
	int* resCirc = getCircles();
	for (int i = 0; i < _params.countPx; i++)
		_marker[i] = sqrt(resStar[i] * resCirc[i]);
	code = _code;
	delete[] resStar;
	delete[] resCirc;
	return _marker;
}