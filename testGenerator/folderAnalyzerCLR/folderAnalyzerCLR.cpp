// folderAnalyzerCLR.cpp: main file of the project.

#include "stdafx.h"
#include <string>
#include <iostream>
#include <time.h>
#include <vector>

using namespace System;
using namespace System::IO;
using namespace System::Diagnostics;
using namespace std;

/**
* Method to count time
* @return string with current time
*/
const std::string currentDateTime() {
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);
	strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

	return buf;
}

int main(array<System::String ^> ^args)
{
	bool cont = 0;
	cout << "\a";
	DateTime begDate = System::DateTime::Now;
	bool readed = 0;
	for each(String^ folder in Directory::GetDirectories(Directory::GetCurrentDirectory(),"x*"))
	{
		begDate = System::DateTime::Now;
		String^ folderName = Path::GetFileName(folder)->Replace("x", "");
		array<String^> ^Result = 
			Directory::GetFiles(folder, "*.bmp", IO::SearchOption::AllDirectories);
		int count = 0;
		String^ tmp = "";
		String^ fileName = "";
		array<String^> ^tmpRes;
		StreamWriter^ wr = 
			gcnew StreamWriter(Directory::GetCurrentDirectory() + "\\P" + folderName + ".txt");
		for (int j = 0; j < Result->Length; j++)
		{
			count = 0;
			readed = 0;
			Process^ pr = gcnew Process();
			pr->StartInfo->FileName = args[0];
			pr->StartInfo->Arguments = Result[j];
			pr->StartInfo->UseShellExecute = false;
			pr->StartInfo->RedirectStandardOutput = true;
			pr->StartInfo->CreateNoWindow = true;
			pr->Start();
			cont = 0;
			while (!pr->StandardOutput->EndOfStream)
			{
				readed = 1;
				tmp = pr->StandardOutput->ReadLine();
				tmpRes = tmp->Split(' ');
				Console::Write(".");
				for each (String^ resCode in tmpRes)
				{
					if (resCode->Equals("")) continue;
					cont = Result[j]->Contains(resCode);
					if (cont) count++;
					cont = 0;
				}
			}
			if (readed)
			{
				// uncomment this part of string if there is multiple markers on image
				tmp = j + 1 + "\t" + Convert::ToDouble((double)count/*/i*/);
				wr->WriteLine(tmp);
			}
		}
		Console::WriteLine("");
		wr->Close();
		Console::WriteLine(begDate + " " + System::DateTime::Now);
		Console::WriteLine(begDate - System::DateTime::Now);
	}
    return 0;
}