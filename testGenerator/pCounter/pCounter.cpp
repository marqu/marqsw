// pCounter.cpp: main file of the project.

#include "stdafx.h"
#include <string>

using namespace System;
using namespace System::IO;
using namespace std;

int main(array<System::String ^> ^args)
{
	System::String^ folder = args[0];
	StreamWriter^ wr = gcnew StreamWriter(folder+"P.csv");
	String^ line = "";
	double P = 0;
	int count = 0;
	double scale = 1;
	for each(String^ file in Directory::GetFiles(folder, "P*.txt"))
	{
		StreamReader^ rd = gcnew StreamReader(file);
		while (!rd->EndOfStream)
		{
			line = rd->ReadLine();
			line = line->Substring(line->IndexOf("\t") + 1);
			P += Convert::ToDouble(line);
			Console::Write(".");
			count++;
		}
		Console::WriteLine("\n");
		rd->Close();
		double toWrite = P / (double)count;
		wr->WriteLine(Path::GetFileName(file)->Replace("P", "")->Replace(".txt", "") + "; " + toWrite + ";");
		P = 0;
		count = 0;
	}
	wr->Close();
    return 0;
}
